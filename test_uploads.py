import uploader


observer_info = {
    1: {
        'ip': '192.168.0.11',
        'image-ids': [1]
    }
}

image_info = {
    1: {
        'path': 'uploads/images/1',
        'type': 'zolertia',
        'os': 'riot'
    }
}

uploader.send_observer_images(observer_info, image_info, 1, 'uploads/tests/1.yaml')
