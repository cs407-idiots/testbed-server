import paho.mqtt.client as mqtt
from dateutil.parser import parse
from datetime import datetime
import time


def parse_log(message):
    """
    Parse a log received via MQTT

    Args:
        message (str): received log

    Returns:
        list of parsed values:
            [date, job_id, node_id, device_serial, mote_id, level, msg]
    """
    [timestr,
     job_id,
     node_id,
     device_serial,
     mote_id,
     level,
     msg] = message.split(';', 6)

    try:
        date = parse(timestr)
    except ValueError:
        print("Error parsing date")

    return [date, job_id, node_id, device_serial, mote_id, level, msg]


def parse_register(message):
    """
    Parse an observer node registration message

    Args:
        message (str): received message
    
    Returns:
        list of parsed values: [id, IP address]
    """
    [_, id, ip] = message.split(" ")
    return [int(id), ip]


class MQTTClient:
    """
    Class to contain MQTT client as well as other objects it uses

    Attributes:
        client (paho.mqtt.client.Client): MQTT client
        influx_db (database.InfluxDatabase): InfluxDB object
        sql_db (database.MySQLDatabase): MySQL database object
        node_register (node_register.NodeRegister): node register
        email_client (email_client.EmailClient): email client
        iot_actions (module): IoT controller module
        scheduler (scheduler.Scheduler): job scheduler
        ready (dict): record of which motes are ready to start a test

    Raises:
        KeyError: invalid configuration
    """
    def __init__(self,
                 config,
                 influx_db,
                 sql_db,
                 node_register,
                 email_client):
        self.client = mqtt.Client()
        self.influx_db = influx_db
        self.sql_db = sql_db
        self.node_register = node_register
        self.email_client = email_client
        
        self.iot_actions = None
        self.scheduler = None
        self.ready = None

        try:
            self.client.connect(config['mqtt']['host'],
                                config['mqtt']['port'],
                                60)
        except KeyError as k:
            print(f"Invalid config: expected key {k.args[0]}")

        # Basic MQTT functions
        def on_connect(client, userdata, flags, rc):
            print("Connected to MQTT broker with result code " + str(rc))
            topics = [
                ("topic/log", 0),
                ("topic/register", 0),
                ("topic/register_motes", 0),
                ("topic/ready", 0),
                ("topic/failed_flash", 0)
            ]
            client.subscribe(topics)

        def on_message(client, userdata, message):
            try:
                self.handle_message(message)

            except Exception as e:
                print("Error in on_message: ", e)
                raise e

        # this is for MQTT errors, *not* LucidLab test errors
        def on_log(client, userdata, level, buf):
            if level == mqtt.MQTT_LOG_ERR:
                print(buf)

        self.client.on_connect = on_connect
        self.client.on_message = on_message
        self.client.on_log = on_log

        self.client.loop_start()

    def set_iot_actions(self, iot_actions):
        """
        Setter for IoT actions module

        Args:
            iot_actions (module): IoT controller module
        """
        self.iot_actions = iot_actions

    def close(self):
        """
        Close client
        """
        self.client.loop_stop()
        self.client.disconnect()

    def handle_message(self, message):
        """
        Call appropriate callback for message topic

        Args:
            message (MQTT message): message received
        """
        topic_handlers = {
            "topic/log": self.on_node_log,
            "topic/register": self.on_register,
            "topic/register_motes": self.on_register_motes,
            "topic/ready": self.on_ready,
            "topic/failed_flash": self.on_failed_flash
        }

        topic = message.topic

        callback = topic_handlers.get(topic, self.on_unhandled)
        callback(message)

    def on_node_log(self, message):
        """
        Handler for sensor mote log received from observer node

        Args:
            message (MQTT message): received log
        
        Raises:
            Exception: error while handling log
        """
        [date,
         job_id,
         node_id,
         device_serial,
         mote_id,
         level,
         msg] = parse_log(message.payload.decode())
        print(f"{date}: NODE: {node_id} DEVICE: {device_serial} MOTE ID: {mote_id}: {msg}")

        if level == "ERROR":
            # send error email
            self.email_client.send_error_email(job_id, msg)
            self.sql_db.mark_job_completed(job_id)
            return
        
        # Manually set to -2 when running default firmware
        if int(job_id) == -2:
            # Strip of unnecessary logging characters
            msg = msg[1:-3]
            
            # Register mote for recording network metrics
            self.node_register.mote_monitor.register_in_rssi_dict(device_serial)
            self.node_register.mote_monitor.register_in_cca_dict(device_serial)
            self.node_register.mote_monitor.register_in_noise_floor_dict(device_serial)

            try:
                if 'my_addr' in msg: #RSSI metrics received
                    mote_addr = msg.split(';')[0][9:]
                    src_addr = msg.split(';')[1][9:]
                    rssi = msg.split(';')[2][5:]
                    self.node_register.mote_monitor.update_rssi_dict(device_serial, mote_addr, src_addr, rssi)

                elif 'cca' in msg: #CCA metrics received
                    msg = msg[9:]
                    cca_values = []

                    for cca in msg.split(';'):
                        cca_values.append(int(cca))

                    self.node_register.mote_monitor.set_cca_dict(device_serial, cca_values)
                
                elif 'noise' in msg:
                    msg = msg[17:]
                    print(msg)
                    noise_floor_values = []

                    for noise_floor in msg.split(';'):
                        noise_floor_values.append(int(noise_floor))
                
                    self.node_register.mote_monitor.set_noise_floor_dict(device_serial, noise_floor_values)
            except (ValueError, IndexError):
                print("Invalid idle log, ignoring (likely jumbled serial output)")

            # Update last contact for the mote
            self.node_register.update_mote_status('idle', device_serial)
            return
        
        # Otherwise, this is a test log
        try:
            self.influx_db.insert_log(job_id,
                                      'MOTE',
                                      node_id,
                                      device_serial,
                                      mote_id,
                                      date,
                                      level,
                                      msg,
                                      '',
                                      '',
                                      '',
                                      '')
            
            # Pass to the IoT controller script if set
            if self.iot_actions:
                self.iot_actions(msg)

        except Exception as e:
            error_msg = (f"Error in on_node_log: {e}")
            print(error_msg)
            self.email_client.send_error_email(job_id, error_msg)

    def on_register(self, message):
        """
        Handler for observer node registration message

        Args:
            message (MQTT message): registration message received

        Raises:
            Exception: error during registration
        """
        try:
            [id, ip] = parse_register(message.payload.decode())
            self.node_register.register_node(id, ip)
        except Exception as e:
            print('Exception in on_register: ' + str(e))
            raise e

        self.node_register.register_node(message)

    def on_register_motes(self, message):
        """
        Handler for mote registration messages. Parse mote serial numbers and 
        interfaces. Check if there are any scheduled jobs: if not, tell the observer 
        that sent the message to flash default firmware to all of its motes. Set those 
        motes to be idle.

        Args:
            message (MQTT message): registration message received
        """
        [node_id, motes, interfaces] = (message.payload.decode()).split(';')
        print(f"NODE ID: {node_id} MOTES: {motes} ON INTERFACES: {interfaces}")
        motelist = {}

        if (motes != '[]'):
            all_motes = motes.replace(' ', '').strip('[]').split(',')
            all_interfaces = interfaces.replace(' ', '').strip('[]').split(',')

            # Build dict
            motelist = dict(zip(all_motes, all_interfaces))
            self.node_register.register_motes(node_id, motelist)
            print(self.node_register.get_registered_motes())
        else:
            self.node_register.register_motes(node_id, {})
            print(self.node_register.get_registered_motes())
        
        # Check for scheduled jobs
        scheduled_jobs = self.sql_db.get_all_jobs(False)
        if len(scheduled_jobs) == 0:
            # Allow time for client to properly initialise
            time.sleep(1)
            self.send_default_run_signal_individual(node_id)
            self.node_register.update_motes_node_idle(node_id)

    def on_failed_flash(self, message):
        """
        Handler for when a mote informs the server that it failed to flash firmware

        Args:
            message (MQTT message): failed flash message received
        """
        mote_id, node_id = message.payload.decode().split(';')
        # Instead of setting ready to True for this observer
        # set it to the failed mote ID
        self.ready[node_id] = str(mote_id)
        # Check to see if all motes are ready
        if (all(value for value in self.ready.values())):
            self.check_all_success()

    def on_unhandled(self, message):
        """
        Handler for unknown topics: print and continue

        Args:
            message (MQTT message): unhandled message received
        """
        print("ERROR: Received message on unhandled topic " +
              message.topic)
        print("Message:")
        print(message.payload.decode())

    def send_start_signal(self, test_id, job_id):
        """
        Tell observer nodes to begin the specified test ID with the specified job ID

        Args:
            test_id (int): test ID to begin
            job_id (int): job_id to assign to logs
        """
        self.ready = dict.fromkeys(self.node_register.register.keys(), False)
        msg = f"{test_id};{job_id}"
        self.client.publish("topic/start_test", msg)

    def send_stop_signal(self, test_id):
        """
        Tell observer nodes to end the specified test ID

        Args:
            test_id (int): test ID to end
        """
        self.client.publish("topic/end_test", test_id)

    def on_ready(self, message):
        """
        Handler for observer nodes reporting ready to start test

        Args:
            message (MQTT message): ready message received
        """
        node_id = message.payload.decode()
        # Mark this observer as ready
        self.ready[node_id] = True
        # Check to see if all observers have responded
        if (all(value for value in self.ready.values())):
            self.check_all_success()
    
    def check_all_success(self):
        """
        Verify whether flashing has succeded on every observer node
        """
        failure = []
        # If any observer ID has a value that is not True specified
        # it has failed to flash for the mote ID that is the value
        for node_id in self.ready.keys():
            if self.ready[node_id] != True:
                failure.append(node_id)
        if len(failure) == 0:
            print("All motes flashed, beginning test")
        else:
            print("Error during flashing, ending test")
        # Tell the scheduler when to end the test, if failure is not
        # empty, it will end the test immediately
        finish_flash_time = datetime.now()
        self.scheduler.stop_timer(finish_flash_time, failure)


    def send_default_run_signal(self):
        """
        Tell all observer nodes to run default firmware on all motes

        Occurs on startup or test end when there are no further tests scheduler
        """
        self.node_register.update_motes_all_idle()
        self.scheduler.schedule_default_firmware()
        self.client.publish("topic/run_default")

    def send_default_run_signal_individual(self, node_id):
        """
        Tell the specified observer node to run default firmware for all motes

        Occurs if no tests are scheduled when an observer registers
        """
        self.node_register.update_motes_node_idle(node_id)
        self.client.publish("topic/run_default_individual", node_id)
