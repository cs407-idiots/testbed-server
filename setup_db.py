from influxdb import InfluxDBClient
import getpass
from subprocess import Popen, PIPE
import mysql.connector
from mysql.connector import Error
import sys
import yaml
import re


def load_config():
    try:
        with open('config.yaml') as file:
            config = yaml.load(file, Loader=yaml.FullLoader)
            return config
    except OSError:
        print("config.yaml not found. "
              "Please create it in the current directory "
              "and re-run the script.")
        sys.exit()


def split_sql_file(text):
    joined = " ".join(text.split("\n"))
    statements = joined.split(";")
    return statements


def run_statement(conn, stmt):
    cursor = conn.cursor()
    cursor.execute(stmt)
    cursor.close()


def y_or_n(prompt):
    while True:
        ans = input(prompt + " (y/n): ")
        if ans and ans[0] == "y":
            return True
        elif ans and ans[0] == "n":
            return False
        else:
            print("Please enter y or n.")


print("Please note that running this script will delete the database"
      " if it exists already.")
ans = y_or_n("Continue?")
if not ans:
    sys.exit()

config = load_config()

# INFLUXDB
print(f"Connecting to InfluxDB on {config['influxdb']['host']}:"
      f"{config['influxdb']['port']}...")
db_name = config['influxdb']['database']
client = InfluxDBClient(host=config['influxdb']['host'],
                        port=config['influxdb']['port'])

print(f"Creating database {db_name}...")
# create database if it doesn't exist already
if not any(d['name'] == db_name for d in client.get_list_database()):
    client.create_database(db_name)

client.create_retention_policy('log_cleanup', '52w', 1,
                               database=db_name,
                               default=True)
print("Successfully created InfluxDB database.")

# MYSQL
# get mysql root password
root_pass = getpass.getpass(prompt='MySQL root password: ')

nonempty = re.compile(r"\S")

print(f"Connecting to MySQL at {config['mysql']['host']}...")
conn = mysql.connector.connect(
    host=config['mysql']['host'],
    user='root',
    password=root_pass)

print("Connected.")

with open('create_database.sql') as f:
    for statement in split_sql_file(f.read()):
        # skip empty statements
        if not nonempty.findall(statement):
            continue
        run_statement(conn, statement)
print('Successfully created database.')

cursor = conn.cursor()
cursor.execute('USE lucidlab')
with open('schema.sql') as f:
    for statement in split_sql_file(f.read()):
        # skip empty statements
        if not nonempty.findall(statement):
            continue
        run_statement(conn, statement)

print('Successfully executed schema.')
