import uploader

observer_info = {2: {'ip': "10.248.12.186", "image-ids":[4]}}
image_info = {4: {
                    'path': '/home/scott/4yp/testbed-server/uploads/images/4.bin',
                    'type': 'zolertia',
                    'os': 'contiki'
                 }
             }
config_path = '/home/scott/4yp/testbed-server/uploads/tests/4.yaml'
id_serial = {1: "ZOL-RM02-B1002315", 2: "ZOL-RM02-B1002295"}
pi_password = 'raspberry'

uploader.send_observer_images(observer_info, image_info, 4, config_path, id_serial, pi_password)