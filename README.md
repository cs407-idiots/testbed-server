# Testbed-server

This is the server side code for the Lucidlab testbed. The main program is run using:

`python server.py`

## Setup
Although some details are specified here, refer to the [Wiki](https://gitlab.com/cs407-idiots/testbed-server/-/wikis/LucidLab-Setup/LucidLab%20Server%20Setup%20Instructions) for detailed guidance.

## Prerequisites

You will need the following:

- A MySQL server running on the default port.
    - In development, MariaDB was used, but any compliant MySQL implementation should work.
    - A database called 'lucidlab'.
    - See the section on database setup for more detail on this.
- An MQTT broker running on the default port. 
    - In development, the [Eclipse Mosquitto MQTT broker](https://mosquitto.org/) was used; we recommend Mosquitto for its stability and speed.
- An InfluxDB server running on the default port.
    - This should have a database named 'logging'. Logs will be written to the 'logging' series.
- Python 3.7. **This is a requirement; the project is not tested on other versions of Python.**
- Pipenv
    - Install the dependencies using `pipenv install`, then enter the virtual environment with `pipenv shell`.
- The ARM embedded compiler toolchain, specifically `arm-none-eabi-objcopy`
    - This is required to convert .elf firmware to hex/bin.
    - Find it by googling it. There are plenty of pre-compiled binaries.
    - The binary is provided in the tools directory *but this is the version for Linux x86_64 ONLY*.
    - The ARM version (and all others, including updates) can be found at [ARM's website](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads).
- An OpenHAB instance
    - To install and configure an OpenHAB instance onto the server, run `./openhab/openhab`.
    - Note this uses the `sudo apt` command, so you'll need to input your root password.

## Background services

Assuming all dependencies are installed and the server system is running a systemd-based OS (Centos 7+ fits this requirement, as does Ubuntu), the background services need to be running for the app to work.
The easiest way to do this is to `enable` the appropriate services via the `systemctl` command:

```
$ sudo systemctl enable influxdb
$ sudo systemctl enable mosquitto
$ sudo systemctl enable mariadb
$ sudo systemctl enable openhab2
```

This will enable the services such that they run at startup, and start them if they're not already running. 

## `config.yaml`
Database hosts, ports, etc. can be configured via the server's `config.yaml` file. The contents of this file match sensible defaults for the various project dependencies.
