#############################################################################
# "iot_actions_template.py":
# This file contains a number of examples of how to interact with IoT 
# devices on openHAB RESTfully. A request to OpenHAB occurs using the 
# `requests` module and follows a pattern of:
#
# `requests.post(<DEVICE> + <CHANNEL>, data=<ACTION>)`
# where:
# - <DEVICE> is the address for an IoT device, such as SONOS
# - <CHANNEL> refers to a setting to be changed on a device, such as CONTROL
# - <ACTION> is the change to perform, such as PLAY or PAUSE
#
# Requests should be performed within the `iot_actions()` function. This
# function includes the `message` parameter which contains the latest log
# output from your firmware. Using this output, you can dynamically
# control the IoT devices. 
#
# !!! `iot_actions()` cannot be renamed, however defining variables and
# functions outside of `iot_actions()` is acceptable.
#############################################################################

import requests

### PHILIPS HUE
## Bulbs:
HUE_BULB_1 = "http://localhost:8081/rest/items/hue_0210_001788a5fa24_1"
HUE_BULB_2 = "http://localhost:8081/rest/items/hue_0210_001788a5fa24_2"
HUE_BULB_3 = "http://localhost:8081/rest/items/hue_0210_001788a5fa24_3"

## COLOUR Channel:
COLOUR = "_color"
# COLOUR Actions:
LIGHT_ON = "ON" 
LIGHT_OFF = "OFF"
RED = "0,100,100"
GREEN = "115,100,100"
BLUE = "235,100,100"

## COLOUR_LOOP Channel:
COLOUR_LOOP = "_effect"
# COLOUR_LOOP Actions:
LOOP_ON = "ON"
LOOP_OFF = "OFF"

### SONOS
## Speaker:
SONOS = "http://localhost:8081/rest/items/sonos_PLAY1_RINCON_7828CAA3EE0801400" # Kitchen Speaker

## CONTROL Channel:
CONTROL = "_control"
# CONTROL Actions:
PLAY = "PLAY"
PAUSE = "PAUSE"
NEXT = "NEXT"

## MUTE Channel:
MUTE = "_mute"
# MUTE Actions:
MUTE_ON = "ON"
MUTE_OFF = "OFF"

## VOLUME Channel:
VOLUME = "_volume"
# VOLUME Actions:
LOW = "20"
MEDIUM = "50"
HIGH = "80"

def iot_actions(message):

    # Turn on HUE_BULB_1
    requests.post(HUE_BULB_1 + COLOUR, data=LIGHT_ON)

    # Set HUE_BULB_1 to red
    requests.post(HUE_BULB_1 + COLOUR, data=RED)

    # Turn off HUE_BULB_2
    requests.post(HUE_BULB_2 + COLOUR, data=LIGHT_OFF)

    # Loop through all colours on HUE_BULB_3
    requests.post(HUE_BULB_3 + COLOUR_LOOP, data=LOOP_ON)

    # Set speaker to play
    requests.post(SONOS + CONTROL, data=PLAY)

    # Mute speaker
    requests.post(SONOS + MUTE, data=MUTE_ON)

    # Set volume to 50
    requests.post(SONOS + VOLUME, data=MEDIUM)