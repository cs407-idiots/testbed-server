#############################################################################
# "iot_actions_example.py":
# This file contains an example of performing an openHAB request based on
# a log output. This example assumes firmware is running on the motes that
# outputs `Temperature=n` at some point during the test, where n is a
# temperature reading from a mote. Whenever this output is seen, a new 
# random colour for a hue bulb is generated and requested. 
#############################################################################

import requests

import random

HUE_BULB_3 = "http://localhost:8081/rest/items/hue_0210_001788a5fa24_3"
COLOUR = "_color"
LIGHT_ON = "ON" 
LIGHT_OFF = "OFF"
HSV = ",100,100"

def iot_actions(message):

    ### MESSAGE PARSING:
    # Strip all whitespace 
    message = "".join(message.split())
    if "=" in message:
        # Split string on equals symbol
        lhs, rhs = message.split("=", 1)

        if "Temperature" in lhs:
            # Use global HSV constant and prepend random hue value
            global HSV
            colour = random.randint(0,360)
            rand_HSV = str(colour) + HSV
            ### OpenHAB request:
            # Set HUE BULB 3 to be a random colour
            requests.post(HUE_BULB_3 + COLOUR, data=rand_HSV)