from flask import request, make_response, flash
from flask import current_app as app
from flask import Blueprint
import yaml
import os
import hashlib
import subprocess as sp
import re
import requests

import validation
import uploader
import iot_logging

routes = Blueprint('routes', __name__)


@routes.route('/request-id')
def route_request_id():
    """
    Gets a new node ID, called by observer nodes.
    
    Fields:
        check: The optional ID for the node.
    """
    proposed_id = request.args.get('check', default=0, type=int)
    ip = request.remote_addr
    return str(app.node_register.get_new_node_id(proposed_id, ip))


@routes.route('/register-observer')
def route_register_observer():
    """
    Registers an observer node. Also retrieves its IP address.
    
    Fields:
        observer_id: The ID of the observer.
    """
    ip = request.remote_addr
    observer_id = request.args.get('observer_id')
    print(f'Registering observer {observer_id} with IP {ip}')

    app.node_register.register_node(observer_id, ip)

    return make_response('Registration successful', 200)


@routes.route('/registered-motes', methods=['GET'])
def route_registered_motes():
    """
    Returns a dictionary of all registered motes.
    """
    try:
        registered = app.node_register.get_registered_motes_dict()

        return make_response({
            'motes': registered
        }, 200)
    except Exception as e:
        err = f'Error getting registered motes: {e}'
        print(err)
        return make_response(err, 500)


@routes.route('/mote-availability', methods=['GET'])
def route_mote_status():
    """
    Returns the statuses of all registered motes.
    """
    try:
        status = app.node_register.get_mote_status()

        return make_response({
            'status': status
        }, 200)
    except Exception as e:
        err = f'Error getting mote statuses: {e}'
        print(err)
        return make_response(err, 500)


@routes.route('/validate-yaml', methods=['POST'])
def route_validate_yaml():
    """
    Validate a test configuration YAML file.
    Can include 'yaml' as a file in the request's files,
    or include 'yaml' as a form field in the POST request.
    
    Files:
     - yaml: The yaml file that is to be validated.
    """
    if request.method == 'POST':
        if 'yaml' in request.files:
            # file upload
            file = request.files['yaml']

            if file.filename == '':
                flash('No selected file')
                return make_response("Upload filename is empty.", 400)

            # Write configuration to file
            uploader.create_upload_folders(app.config['UPLOAD_FOLDER'])
            if file and validation.allowed_config_filename(file.filename):
                path = os.path.join(app.config['UPLOAD_FOLDER'], file.filename)
                file.save(path)
                valid = False
                error = None

                with open(path, 'r') as f:
                    request_yaml = yaml.safe_load(f)
                    valid, error = validation.validate_yaml_format(
                        request_yaml
                    )

                os.remove(path)
                if valid:
                    return make_response("Valid YAML config.", 200)
                return make_response("Invalid uploaded config.\n" + error, 400)
        elif 'yaml' in request.form:
            # uploaded directly
            request_yaml = yaml.safe_load(request.form.get('yaml'))
            valid, error = validation.validate_yaml_format(request_yaml)
            if valid:
                return make_response("Valid YAML config.", 200)
            return make_response("Invalid POST config.\n" + error, 400)

        flash('Missing YAML file.')
        return make_response(
            "Missing YAML in request. Either provide the "
            "'yaml' form field, or attach a file named 'yaml'.",
            400)

    return make_response("Expected POST request.", 400)


@routes.route('/create-user', methods=['POST'])
def route_create_user():
    """
    Create a new user in the MySQL database.
    
    Fields:
        username: The desired username.
        email: The email of the user signing up.
        password: The password to be used.
    """
    if 'username' not in request.form:
        return make_response('Missing username.', 400)
    username = request.form.get('username')

    if 'email' not in request.form:
        return make_response('Missing username.', 400)
    email = request.form.get('email')

    if 'password' not in request.form:
        return make_response('Missing username.', 400)
    password = request.form.get('password')

    try:
        id = app.mysql_db.create_user(username, email, password)

        return make_response({
            'message': 'User creation successful.',
            'id': id
        }, 200)
    except Exception as e:
        return make_response(e, 500)
    return 'OK'


@routes.route('/test-login', methods=['POST'])
def route_test_login():
    """
    Test a username/password combination for login.
    
    Fields:
        username: The username for the login attempt.
        password: The password dfor the login attempt.
    """
    if 'username' not in request.form:
        return make_response('Missing username.', 400)
    username = request.form.get('username')

    if 'password' not in request.form:
        return make_response('Missing password.', 400)
    password = request.form.get('password')

    success, user_id = app.mysql_db.query_user_login(username, password)

    if success:
        return make_response({
            'message': 'Login successful.',
            'user_id': user_id
        }, 200)
    return make_response('Username/password combination incorrect.', 403)


@routes.route('/upload-configuration', methods=['POST'])
def route_upload_configuration():
    """
    Upload a test configuration.
    
    Fields:
        checksum: A SHA1 checksum for the uploaded test configuration. If 
                  provided, the upload will be checked against this.
        user_id: The ID of the user that uploaded the image.
    
    Files:
        yaml: The YAML file to be uploaded
    """
    if request.method != 'POST':
        return make_response('Expected POST request.', 400)

    # get various form variables; some of them are required
    checksum = request.form.get('checksum')

    if 'user_id' not in request.form:
        return make_response('Missing user ID.', 400)
    user_id = request.form.get('user_id')

    if 'yaml' not in request.files:
        return make_response('Missing YAML in request.', 400)

    file = request.files['yaml']

    if file.filename == '':
        flash('No selected file')
        return make_response('Upload filename is empty.', 400)

    file_text = file.read()

    # if the uploader provided a checksum, check it matches.
    # if they didn't, get the hash of the file for use in the database.
    sum_hash = hashlib.sha1(file_text).hexdigest()
    if checksum:
        if sum_hash != checksum:
            return make_response('Uploaded file does not match checksum!',
                                 400)
    else:
        checksum = sum_hash

    if not validation.allowed_config_filename(file.filename):
        return make_response('Invalid config filename. Expected *.yaml.', 400)

    valid = False
    error = None

    try:
        parsed_yaml = yaml.safe_load(file_text)
    except yaml.parser.ParserError as e:
        return make_response('Invalid yaml syntax: ' + str(e), 400)

    # Perform basic validation on fields
    valid, error = validation.validate_yaml_format(parsed_yaml)
    if not valid:
        return make_response('Invalid uploaded config.\n' + error, 400)

    # This checks the list of image IDs to ensure they exist
    images_ok_list, remaining_image_ids = app.mysql_db.check_images_uploaded(
        parsed_yaml['image-ids']
    )

    # This checks for any other image IDs referenced elsewhere in the config to make sure
    # they exist
    referenced_images = [reference['image-id'] for reference in parsed_yaml['image-devices']]
    images_ok_referenced, remaining_image_ids_referenced = app.mysql_db.check_images_uploaded(
       referenced_images
    )

    # Report back any missing images
    remaining_image_ids = list(set(remaining_image_ids) | set(remaining_image_ids_referenced))
    if (not images_ok_list) or (not images_ok_referenced):
        return make_response("Some of the images IDs specified were not found: " + 
                             str(remaining_image_ids), 400)
    
    # Ensure IoT controller exists
    if 'iot-controller-id' in parsed_yaml:
        result = app.mysql_db.check_iot_controller_uploaded(parsed_yaml['iot-controller-id'])
        if not result:
            return make_response("Referenced IoT controller not found:"
                                f"{parsed_yaml['iot-controller-id']}", 400)
    
    # Gather a list of all mentioned mote IDs
    image_motes = {}
    all_motes = []
    for image_device in parsed_yaml['image-devices']:
        image_motes[image_device['image-id']] = [i for i in image_device['mote-ids']]
        all_motes.extend([i for i in image_device['mote-ids']])
    
    # Ensure every mentioned mote is registered with the server
    motes_registered, unregistered = app.node_register.check_motes_registered(all_motes)
    if not motes_registered:
        return make_response("Can't find some motes in this configuration: " +
                             str(unregistered), 400)

    # Retrieve the platform for each mote mentioned
    mote_platforms = app.node_register.get_mote_platform()

    # Compare the mote platforms with the platforms of the images specified images for consistency
    consistent_platforms, inconsistency = app.mysql_db.check_image_platform(image_motes, mote_platforms)
    if not consistent_platforms:
        return make_response("Inconsistent image platform for mote: "
                            f"image {inconsistency[0]} is compiled for {inconsistency[1]} "
                            f"but mote {inconsistency[2]} is a {inconsistency[3]}", 400)

    name = parsed_yaml.get('test-name', '')
    description = parsed_yaml.get('test-description', '')
    duration = int(parsed_yaml['duration'])

    # get an ID for this configuration and store it in the database.
    try:
        config_id = app.mysql_db.create_configuration(
            user_id=user_id,
            checksum=checksum,
            name=name,
            description=description,
            duration=duration
        )
    except Exception as e:
        print('Error creating test config: ', e)
        return make_response('Error when creating test config in database: ' +
                             str(e), 500)

    uploader.create_upload_folders(app.config['UPLOAD_FOLDER'])
    folder_path = os.path.join(app.config['UPLOAD_FOLDER'], 'tests')
    if not os.path.isdir(folder_path):
        print('Creating tests upload folder...')
        os.mkdir(folder_path)

    try:
        with open(os.path.join(folder_path, str(config_id) + '.yaml'),
                  'w') as f:
            yaml.safe_dump(parsed_yaml, f)
    except Exception as e:
        return make_response("Error while writing configuration: " + str(e),
                             400)

    try:
        scheduled_time, job_id = app.scheduler.create_job(config_id)

        return make_response({
            'message': "Configuration upload successful.",
            'id': config_id,
            'scheduled_time': str(scheduled_time),
            'job_id': job_id
        }, 200)
    except Exception as e:
        err = 'Error while scheduling job: ' + str(e)
        print(err)
        return make_response(err, 500)


@routes.route('/get-test-config-file', methods=['GET'])
def route_get_test_config_file():
    """
    Retrieves the test configuration (YAML) file associated with a specified 
    job or test.
    
    Fields:
        job_id: The ID of the requested job.
        test_id: The ID of the requested test.
    """
    if 'job_id' not in request.args and 'test_id' not in request.args:
        return make_response(
            'Expected either test_id or job_id param in request.',
            400
        )
    job_id = request.args.get('job_id')
    test_id = request.args.get('test_id')
    if test_id is None and job_id:
        test_id = app.mysql_db.get_test_id(job_id)
    filename = f'{test_id}.yaml'

    try:
        path = os.path.join(
            app.config['UPLOAD_FOLDER'],
            'tests',
            filename
        )

        with open(path, 'r') as f:
            resp = make_response(f.read())
            resp.headers.set('Content-Type', 'text/yaml')
            # tells browser to download it directly rather than opening inline
            resp.headers.set('Access-Control-Expose-Headers',
                             'Content-Disposition')
            resp.headers.set('Content-Disposition',
                             'attachment', filename=f"config-{filename}")

            return resp
    except IOError:
        return make_response(f'Job {job_id} '
                             'does not exist', 404)
    except Exception as e:
        err = f'Error retrieving job ID {filename}: {e}'
        print(err)

        return make_response(err, 500)
        

@routes.route('/delete-test', methods=['GET'])
def route_delete_test():
    """
    Deletes a test configuration.
    
    Fields:
        test_id: The ID of the test config to delete.
    """
    if 'test_id' not in request.args:
        return make_response('Missing test_id argument.', 400)
    test_id = request.args.get('test_id')

    try:
        app.mysql_db.delete_configuration(test_id)
        path = os.path.join(app.config['UPLOAD_FOLDER'],
                            'tests',
                            str(test_id) + '.yaml')
        os.remove(path)
        return make_response(f'Test {test_id} deleted successfully', 200)
    except Exception as e:
        err = 'Error deleting test: ' + str(e)
        print(err) #Error gets printed in the response
        return make_response(err, 500)


@routes.route('/upload-image', methods=['POST'])
def route_upload_image():
    """
    Uploads an image file.
    
    Fields:
        checksum: A SHA1 checksum for the uploaded image. If provided, the 
                  upload will be checked against this.
        user_id: The ID of the user uploading the image.
        image_type: The board type for this image. e.g. 'zolertia'
        image_os: the OS of the uploaded image. e.g. 'riot' or 'contiki'
        image_name: The name for the image; this is purely cosmetic
                    and is used to identify the image in lists of uploads.
        image_description: As with image_name, this is purely cosmetic.

    Files:
        image: the image file to upload.
    """
    if request.method != 'POST':
        return make_response("Expected POST request.", 400)

    # get various form variables; some of them are required
    checksum = request.form.get('checksum')

    if 'user_id' not in request.form:
        return make_response('Missing user ID.', 400)
    user_id = request.form.get('user_id')

    if 'image_type' not in request.form:
        return make_response('Missing image_type', 400)
    image_type = request.form.get('image_type')

    if 'image_os' not in request.form:
        return make_response('Missing image_os', 400)
    image_os = request.form.get('image_os')

    if 'image_name' not in request.form:
        return make_response('Missing image name')
    image_name = request.form.get('image_name')

    if 'image_description' not in request.form:
        return make_response('Missing image description')
    image_description = request.form.get('image_description')

    if 'image' not in request.files:
        return make_response('Missing image file in request.', 400)

    file = request.files['image']

    if file.filename == '':
        flash('No selected file')
        return make_response('Upload filename is empty.', 400)

    # check if we're running on an ARM architecture
    system = None
    if os.uname()[4][:3].lower().startswith('arm'):
        system = 'armhf'
    else:
        system = 'x86'

    # get file extension
    image_format = file.filename.rsplit('.', 1)[1].lower()

    if not (image_format == 'bin' or image_format == 'hex' or
            image_format == 'ihex' or image_format == 'elf'):
        return make_response('Image format error: ' + image_format +
                             ' is not a valid image format', 400)

    image_type = request.form['image_type']

    file_bytes = file.read()

    # if the uploader provided a checksum, check it matches.
    # if they didn't, get the hash of the file for use in the database.
    sum_hash = hashlib.sha1(file_bytes).hexdigest()
    if checksum:
        if sum_hash != checksum:
            return make_response('Uploaded file does not match checksum!',
                                 400)
    else:
        checksum = sum_hash

    # get an ID for this image and store it in the database.
    try:
        image_id = app.mysql_db.create_image(
            user_id=user_id,
            checksum=checksum,
            image_type=image_type,
            image_os=image_os,
            image_name=image_name,
            image_description=image_description
        )
    except Exception as e:
        print('Error creating test config: ', e)
        return make_response('Error when creating image entry in database: ' +
                             str(e), 500)

    uploader.create_upload_folders(app.config['UPLOAD_FOLDER'])
    folder_path = os.path.join(app.config['UPLOAD_FOLDER'], 'images')
    if not os.path.isdir(folder_path):
        print('Creating images upload folder...')
        os.mkdir(folder_path)

    filename = str(image_id) + '-' + image_os + '-' + image_type

    try:
        with open(os.path.join(folder_path,
                               filename + '.' + image_format),
                  'wb') as f:
            f.write(file_bytes)
        if image_type == 'zolertia':
            if image_format == 'elf':
                # if we have an elf, convert it to hex first
                binary = "arm-none-eabi-objcopy-" + system
                elf_path = os.path.join(folder_path, filename + '.elf')
                hex_path = os.path.join(folder_path, filename + '.hex')
                sp.call('tools/' + binary + ' -O ihex ' +
                        elf_path + ' ' + hex_path, shell=True)
                sp.call('rm ' + elf_path, shell=True)
        elif image_type == 'telosb':
            binary = "msp430-objcopy-" + system
            hex_path = os.path.join(folder_path, filename + '.hex')
            if image_format == 'elf':
                elf_path = os.path.join(folder_path, filename + '.elf')
                sp.call('tools/' + binary + ' -I elf32-msp430 -O ihex ' +
                        elf_path + ' ' + hex_path, shell=True)
                sp.call('rm ' + elf_path, shell=True)
            elif image_format == 'bin':
                bin_path = os.path.join(folder_path, filename + '.bin')
                sp.call('tools/' + binary + ' -I binary -O ihex --change-addresses=0x4000 ' +
                        bin_path + ' ' + hex_path, shell=True)
                sp.call('rm ' + bin_path, shell=True)

    except sp.CalledProcessError:
        return make_response('Error during image file format conversion. ' +
                             'Ensure image is correctly compiled', 400)
    except Exception as e:
        return make_response('Error while writing image: ' + str(e), 400)

    return make_response({
        'message': 'Image upload successful.',
        'id': image_id
    }, 200)


@routes.route('/delete-image', methods=['GET'])
def route_delete_image():
    """
    Deletes an image from the server.
    
    Fields:
        image_id: The ID of the image to delete.
    """
    if 'image_id' not in request.args:
        return make_response('Missing image_id argument.', 400)
    image_id = request.args.get('image_id')

    try:
        app.mysql_db.delete_image(image_id)
        path = os.path.join(app.config['UPLOAD_FOLDER'], 'images')
        img_regex = re.compile(f'{image_id}.+')
        for filename in os.listdir(path):
            if img_regex.match(filename):
                os.remove(os.path.join(path, filename))

                return make_response(f'Image {image_id} deleted successfully',
                                     200)
            else:
                return make_response(f'Image with id {image_id} was not found',
                                     200)
    except Exception as e:
        print('Error deleting image: ' + str(e))
        return make_response('Error deleting image: ' + str(e), 500)
    return 'OK'


@routes.route('/download-image', methods=['GET'])
def route_download_image():
    """
    Retrieves the image as a file and downloads it.
    
    Fields:
        image_id: 
    """
    if 'image_id' not in request.args:
        return make_response('Missing image_id argument.', 400)
    image_id = request.args.get('image_id')

    try:
        path = os.path.join(app.config['UPLOAD_FOLDER'], 'images')
        img_regex = re.compile(f'{image_id}.+')
        for filename in os.listdir(path):
            if img_regex.match(filename):
                with open(os.path.join(path, filename), 'rb') as f:
                    img_binary = f.read()
                    resp = make_response(img_binary)
                    resp.headers.set('Access-Control-Expose-Headers',
                                     'Content-Disposition')
                    resp.headers.set('Content-Type',
                                     'application/octet-stream')
                    resp.headers.set('Content-Disposition',
                                     'attachment', filename=filename)
                    return resp
        return make_response(f'Image {image_id} does not exist', 400)
    except Exception as e:
        err = 'Error retrieving image: ' + str(e)
        print(err)
        return make_response(err, 500)


@routes.route('/check-images-uploaded', methods=['GET'])
def route_check_images_uploaded():
    """
    For a given test configuration, check that all the images it requires are
    present in the database. Part of the pre-flash validation.
    
    Fields:
        config_id: The ID of the test configuration to check.
    """
    if 'config_id' not in request.args:
        return make_response("Missing config_id argument.", 400)
    config_id = request.args.get('config_id')

    required_ids = uploader.get_config_image_ids(app.config['UPLOAD_FOLDER'],
                                                 config_id)
    success, remaining = app.mysql_db.check_images_uploaded(required_ids)

    if success:
        return make_response({
            "valid": True,
        }, 200)
    return make_response({
        "valid": False,
        'message': 'Some images were not found. See missing_ids for info.',
        'missing_ids': remaining
    }, 404)


@routes.route('/user-configurations', methods=['GET'])
def route_get_user_configurations():
    """
    Get a list of all uploaded configurations for a given user.
    
    Fields:
        user_id: The ID of the user that requests the configurations.
    """
    if 'user_id' not in request.args:
        return make_response("Missing user_id argument.", 400)
    user_id = int(request.args.get('user_id'))

    data = app.mysql_db.get_user_configurations(user_id)

    return make_response(data, 200)


@routes.route('/user-images', methods=['GET'])
def route_get_user_images():
    """
    Get a list of uploaded images and associated metadata for a given user.
    
    Fields:
        user_id: The ID of the user that requests the images.
    """
    if 'user_id' not in request.args:
        return make_response('Missing user_id argument.', 400)
    user_id = int(request.args.get('user_id'))

    images_info = app.mysql_db.get_user_images(user_id)

    return make_response({
        'images': images_info
    }, 200)


@routes.route('/create-job', methods=['GET'])
def route_create_job():
    """
    Create a job. Will schedule a slot for the given test.
    
    Fields:
        test_id: The ID of the test which will be scheduled in a new job.
    """
    if 'test_id' not in request.args:
        return make_response('Missing test_id argument.', 400)
    test_id = int(request.args.get('test_id'))

    path_exists = os.path.exists(os.path.join(app.config['UPLOAD_FOLDER'],
                                              'tests',
                                              str(test_id) + '.yaml'))
    if not path_exists:
        return make_response(f'Test {test_id} has not been uploaded.', 400)
    scheduled_time, job_id = app.scheduler.create_job(test_id)

    return make_response({
        'message': f'Successfully scheduled test run for {scheduled_time}.',
        'job_id': job_id
    }, 200)


@routes.route('/get-job-results-json', methods=['GET'])
def routes_get_job_results_json():
    """
    Download the results of a job as a JSON.
    
    Fields:
        job_id: The ID of the requested job.
    """
    if 'job_id' not in request.args:
        return make_response("Missing job_id argument.", 400)
        
    job_id = int(request.args.get('job_id'))
    job_info = app.mysql_db.get_job_info(job_id)
    
    if not job_info:
        return make_response(f'Job with ID {job_id} does not exist', 400)
    if not job_info['completed']:
        return make_response(f'Job with ID {job_id} has not finished running',
                             400)

    logs = app.influx_db.retrieve_job_logs(job_id)

    return make_response({
        'logs': logs
    }, 200)


@routes.route('/get-job-results-csv', methods=['GET'])
def routes_get_job_results_csv():
    """
    Download the results of a job as a CSV.
    
    Fields:
        job_id: The ID of the requested job.
    """
    if 'job_id' not in request.args:
        return make_response("Missing job_id argument.", 400)
    job_id = int(request.args.get('job_id'))
    job_info = app.mysql_db.get_job_info(job_id)
    if not job_info:
        return make_response(f'Job with ID {job_id} does not exist', 400)
    if not job_info['completed']:
        return make_response(f'Job with ID {job_id} has not finished running',
                             400)

    csv = app.influx_db.retrieve_job_logs_csv(job_id)

    return make_response(csv, 200)


@routes.route('/get-test-info', methods=['GET'])
def routes_get_test_info():
    """
    Returns a dictionary of metadata about a test.
    
    Fields:
        test_id: The ID of the requested test.
        job_id: The ID of the job associated with the requested test.
    """
    test_id = None
    if 'test_id' in request.args:
        test_id = request.args.get('test_id')
    elif 'job_id' in request.args:
        job_id = request.args.get('job_id')
        test_id = app.mysql_db.get_test_id(job_id)
        if not test_id:
            return make_response(f'No test run with ID {job_id}', 400)
    else:
        return make_response('Expected either test_id or job_id in request',
                             400)

    try:
        test_config_raw = uploader.get_config_raw(
            app.config['UPLOAD_FOLDER'],
            test_id
        )
        (test_name,
         test_description,
         test_duration) = app.mysql_db.get_test_info(test_id)
        return make_response({
            'config_raw': test_config_raw,
            'test_id': test_id,
            'test_name': test_name,
            'test_description': test_description,
            'test_duration': test_duration
        }, 200)
    except Exception as e:
        err = 'Error retrieving test config: ' + str(e)
        print(err)
        return make_response(err, 500)


@routes.route('/delete-job')
def route_delete_job():
    """
    Deletes a job from the databases.
    
    Fields:
        job_id: The ID of the job that is requested to be deleted.
    """
    if 'job_id' not in request.args:
        return make_response('Missing job_id argument.', 400)
    job_id = request.args.get('job_id')

    try:
        app.mysql_db.delete_job(job_id)
        app.influx_db.delete_job(job_id)
        return make_response(f'Job {job_id} deleted successfully',
                             200)
    except Exception as e:
        err = 'Error deleting job: ' + str(e)
        print(err)
        return make_response(err, 500)


@routes.route('/get-all-jobs')
def routes_get_all_jobs():
    """
    Retrieves the IDs of all jobs from the database
    
    Fields:
        completed: Flag that filters jobs by whether they're completed.
    """
    completed = request.args.get('completed')

    try:
        jobs = None
        if completed == 'true':
            jobs = app.mysql_db.get_all_jobs(True)
        elif completed == 'false':
            jobs = app.mysql_db.get_all_jobs(False)
        else:
            jobs = app.mysql_db.get_all_jobs()

        return make_response({
            'jobs': jobs
        }, 200)
    except Exception as e:
        err = 'Error fetching all jobs: ' + str(e)
        print(err)
        return make_response(err, 500)


@routes.route('/get-user-jobs')
def routes_get_user_jobs():
    """
    Retrieves the IDs of all jobs from the database associated with a user.
    
    Fields:
        user_id: The ID of the requested user.
    """
    if 'user_id' not in request.args:
        return make_response('Missing user_id argument', 400)
    user_id = request.args.get('user_id')

    try:
        jobs = app.mysql_db.get_user_jobs(user_id)
        return make_response({
            'jobs': jobs
        }, 200)
    except Exception as e:
        err = 'Error fetching all jobs: ' + str(e)
        print(err)
        return make_response(err, 500)


@routes.route('/get-user-completed-jobs')
def routes_get_user_completed_jobs():
    """
    Retrieves the IDs of all completed jobs from the database, associated with
    a specific user.
    
    Fields:
        user_id: The ID of the requested user.
    """    
    if 'user_id' not in request.args:
        return make_response('Missing user_id argument', 400)
    user_id = request.args.get('user_id')

    try:
        jobs = app.mysql_db.get_user_jobs(user_id, completed=True)
        return make_response({
            'jobs': jobs
        }, 200)
    except Exception as e:
        err = 'Error fetching completed jobs: ' + str(e)
        print(err)
        return make_response(err, 500)


@routes.route('/get-user-scheduled-jobs')
def routes_get_user_scheduled_jobs():
    """
    Retrieves the IDs of all scheduled jobs from the database, associated with
    a specific user.
    
    Fields:
        user_id: The ID of the requested user.
    """    
    if 'user_id' not in request.args:
        return make_response('Missing user_id argument', 400)
    user_id = request.args.get('user_id')

    try:
        jobs = app.mysql_db.get_user_jobs(user_id, completed=False)
        return make_response({
            'jobs': jobs
        }, 200)
    except Exception as e:
        err = 'Error fetching scheduled jobs: ' + str(e)
        print(err)
        return make_response(err, 500)


@routes.route('/upload-iot-controller', methods=['POST'])
def route_upload_iot_controller():
    """
    Uploads an IoT controller to the server and adds an entry to the database.
    
    Fields:
        checksum: A SHA1 checksum for the uploaded IoT controller. If provided, 
                  the upload will be checked against this.
        user_id: The ID of the user uploading the file.
        name: The cosmetic name associated with the uploaded IoT controller.
        description: The cosmetic description associated with the uploaded IoT
                     controller.
    
    Files:
        controller: The Python script which constitutes the IoT controller.
    """    
    if request.method != 'POST':
        return make_response('Expected POST request.', 400)

    # get various form variables; some of them are required
    checksum = request.form.get('checksum')

    if 'user_id' not in request.form:
        return make_response('Missing user ID.', 400)
    user_id = request.form.get('user_id')

    if 'name' not in request.form:
        return make_response('Missing IoT controller name.', 400)
    name = request.form.get('name')

    if 'description' not in request.form:
        return make_response('Missing IoT controller description.', 400)
    description = request.form.get('description')

    if 'controller' not in request.files:
        return make_response('Missing python controller file in request.', 400)

    file = request.files['controller']

    if file.filename == '':
        flash('No selected file')
        return make_response('Upload filename is empty.', 400)

    file_text = file.read()

    # if the uploader provided a checksum, check it matches.
    # if they didn't, get the hash of the file for use in the database.
    sum_hash = hashlib.sha1(file_text).hexdigest()
    if checksum:
        if sum_hash != checksum:
            return make_response('Uploaded file does not match checksum!',
                                 400)
    else:
        checksum = sum_hash

    if not validation.allowed_iot_controller_filename(file.filename):
        return make_response('Invalid config filename. Expected *.py.', 400)

    # get an ID for this configuration and store it in the database.
    try:
        iot_controller_id = app.mysql_db.create_iot_controller(
            user_id=user_id,
            checksum=checksum,
            name=name,
            description=description
        )
    except Exception as e:
        print('Error creating iot controller: ', e)
        return make_response('Error when creating IoT controller in database: '
                             + str(e), 500)

    # ensure folders exist
    uploader.create_upload_folders(app.config['UPLOAD_FOLDER'])
    folder_path = os.path.join(app.config['UPLOAD_FOLDER'], 'iot_controllers')

    try:
        with open(os.path.join(folder_path, str(iot_controller_id) + '.py'),
                  'wb') as f:
            f.write(file_text)
    except Exception as e:
        return make_response("Error while writing IoT controller: " + str(e),
                             400)

    return make_response({
        'message': "IoT controller upload successful.",
        'id': iot_controller_id
    }, 200)


@routes.route('/download-iot-controller', methods=['GET'])
def route_download_iot_controller():
    """
    Downloads a specified IoT controller file from the server.
    
    Fields:
        iot_controller_id: The ID of the IoT controller requested to be 
                           downloaded.
    """   
    if 'iot_controller_id' not in request.args:
        return make_response('Missing iot_controller_id param in request.',
                             400)
    iot_controller_id = request.args.get('iot_controller_id')
    filename = f'{iot_controller_id}.py'

    try:
        path = os.path.join(
            app.config['UPLOAD_FOLDER'],
            'iot_controllers',
            filename
        )

        with open(path, 'r') as f:
            resp = make_response(f.read())
            resp.headers.set('Content-Type', 'text/plain')
            # tells browser to download it directly rather than opening inline
            resp.headers.set('Access-Control-Expose-Headers',
                             'Content-Disposition')
            resp.headers.set('Content-Disposition',
                             'attachment', filename=f"iot-controller-{filename}")

            return resp
    except IOError:
        return make_response(f'IoT controller {iot_controller_id} '
                             'does not exist', 404)
    except Exception as e:
        err = f'Error retrieving IoT controller {filename}: {e}'
        print(err)

        return make_response(err, 500)


@routes.route('/delete-iot-controller', methods=['GET'])

def route_delete_iot_controller():
    """
    Deletes an IoT controller from the server and the database.
    
    Fields:
        iot_controller_id: The ID of the IoT controller requested to be deleted.
    """   
    if 'iot_controller_id' not in request.args:
        return make_response('Missing iot_controller_id param in request.',
                             400)
    iot_controller_id = request.args.get('iot_controller_id')

    try:
        app.mysql_db.delete_iot_controller(iot_controller_id)

        path = os.path.join(
            app.config['UPLOAD_FOLDER'],
            'iot_controllers',
            f'{iot_controller_id}.py')
        os.remove(path)

        return make_response('Successfully deleted IoT controller '
                             f'{iot_controller_id}', 200)
    except IOError:
        return make_response(f'IoT controller {iot_controller_id} '
                             'does not exist', 404)
    except Exception as e:
        err = 'Error deleting IoT controller: ' + str(e)
        print(err)
        return make_response(err, 500)


@routes.route('/user-iot-controllers')
def route_get_user_iot_controllers():
    """
    Retrieves metadata about all IoT controllers uploaded by a specified user.
    
    Fields:
        user_id: The ID of the user requesting the IoT controller metadata.
    """   
    if 'user_id' not in request.args:
        return make_response('Missing user_id param in request.',
                             400)
    user_id = request.args.get('user_id')

    try:
        iot_controller_info = app.mysql_db.get_iot_controllers(user_id)

        return make_response({
            'iot_controllers': iot_controller_info
        })
    except Exception as e:
        err = f'Error retrieving IoT controller info: {e}'
        print(e)
        return make_response(err, 500)


@routes.route('/get-iot-things')
def route_get_iot_things():
    """
    Returns a dictionary of all smart devices connected through openHAB.
    """       
    try:
        with open('config.yaml') as file:
            config = yaml.load(file, Loader=yaml.FullLoader)

            OPENHAB_URL = 'http://' + config['openhab']['host'] + ':' + str(config['openhab']['port'])

            url = OPENHAB_URL + '/rest/items'
            params = {'fields': 'name, label'}

            r = requests.get(url=url, params=params).json()
            d = {}

            for thing in r:
                name = thing['name']
                d[name] = iot_logging.get_pretty_name(name) + ': ' +\
                    thing['label']

        return make_response(d, 200)

    except Exception as e:
        err = f'Error retrieving IoT things: {e}'
        print(e)
        return make_response(err, 500)

@routes.route('/get-rssi-map')
def route_get_rssi_map():
    """
    Returns a dictionary that represents the RSSI values corresponding to any 
    pair of connected motes.
    """   
    try:
        return make_response({
            'map': app.node_register.mote_monitor.get_rssi_dict()
            }, 200)
    except Exception as e:
        err = f'Error receiving RSSI map: {e}'
        print(err)
        return make_response(err, 500)

@routes.route('/get-cca-data')
def route_get_cca_data():
    """
    Returns average CCA values for a specified mote, alongside the global 
    averages across all motes.
    
    Fields:
        mote_id: The ID of the requested mote.
    """   
    if 'mote_id' not in request.args:
        return make_response('Missing mote_id param in request.',
                            400)
    mote_id = request.args.get('mote_id')
    try:
        mote_id = request.args.get('mote_id')
        
        if mote_id == '':
            mote_response = []
        else:
            mote_response = app.node_register.mote_monitor.get_cca_mote_averages(int(mote_id))

        return make_response({
            'mote': mote_response,
            'global': app.node_register.mote_monitor.get_cca_global_averages()
        }, 200)
    except Exception as e:
        err = f'Error receiving CCA data: {e}'
        print(err)
        return make_response(err, 500)

@routes.route('/get-noise-floor-data')
def route_get_noise_floor_data():
    """
    Returns average noise floor values across the channels of a specified mote,
    alongside the global averages across all motes.
    
    Fields:
        mote_id: The ID of the requested mote.
    """   
    if 'mote_id' not in request.args:
        return make_response('Missing mote_id param in request.',
                            400)
    mote_id = request.args.get('mote_id')
    try:
        mote_id = request.args.get('mote_id')
        
        if mote_id == '':
            mote_response = []
        else:
            mote_response = app.node_register.mote_monitor.get_noise_floor_mote_averages(int(mote_id))

        return make_response({
            'mote': mote_response,
            'global': app.node_register.mote_monitor.get_noise_floor_global_averages()
        }, 200)
    except Exception as e:
        err = f'Error receiving noise floor data: {e}'
        print(err)
        return make_response(err, 500)
