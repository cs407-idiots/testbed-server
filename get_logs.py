import argparse
import os
from influxdb import InfluxDBClient

parser = argparse.ArgumentParser(description='Parse arguments for database query script')
parser.add_argument("--query", required=False)


if __name__ == '__main__':
    args = parser.parse_args()

    if args.query is not None:
        client = InfluxDBClient(host='localhost', port=8086)
        client.switch_database('logging')

        result = client.query(args.query)
        for row in result:
            for entry in row:
                print(entry)
