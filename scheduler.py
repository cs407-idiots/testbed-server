from datetime import timedelta, datetime
from threading import Timer
import os
import uploader
import errno
import glob
import time

from importlib import import_module  # loading in iot controllers
import iot_logging


class Scheduler:
    """
    Job scheduler class

    Attributes:
        config (dict): contents of config.yaml
        influxdb (database.InfluxDatabase): InfluxDB object
        mysql (database.MySQLDatabase): MySQL database object
        node_register (node_register.NodeRegister): node register
        mqtt_client (paho.mqtt.client.Client): MQTT client
        email_client (email_client.EmailClient): email client
        padding (int): time between tests
        buffer (int): buffer for starting tests

        next_slot (datetime.datetime): next available slot
        upload_path (str): where to upload files to
        pi_password (str): password for RPi observers
    """
    def __init__(self,
                 config,
                 influxdb,
                 mysql,
                 node_register,
                 mqtt_client,
                 email_client,
                 padding,
                 buffer):
        self.influxdb = influxdb
        self.mysql = mysql
        self.node_register = node_register
        self.mqtt_client = mqtt_client
        self.email_client = email_client

        # Time in seconds as padding before and after tests
        self.padding = padding
        self.buffer = buffer
        # Initialise next slot variable
        self.next_slot = self.get_current_time() #self.init_slot()
        self.upload_path = config['full-upload-path']
        self.pi_password = config['pi-password']

        self.current_job = {
            "job_id": None,
            "test_id": None,
            "duration": None,
            "start_time": None,
        }

    def create_job(self, config_id):
        """
        Schedule a new job

        Args:
            config_id (int): ID of test configuration

        Raises:
            Exception: error when sending scheduling email
        """
        scheduled_time = self.get_slot(config_id)

        # Schedule job in MySQL DB
        job_id = self.mysql.create_job(config_id, scheduled_time)

        now = datetime.now()
        delay = (scheduled_time - now).total_seconds()
        print("Delay:", delay)

        parsed_config = uploader.get_config_data(self.upload_path, config_id)

        # Update last used values for data retention
        if 'iot-controller-id' in parsed_config:
            self.mysql.update_iot_controller_date_last_used(parsed_config['iot-controller-id'])

        for image_id in parsed_config['image-ids']:
            self.mysql.update_images_date_last_used(image_id)

        # Set a timer to start the test
        start_timer = Timer(delay, lambda: self.start_job(job_id,
                                                          scheduled_time))
        start_timer.start()

        try:
            self.email_client.send_scheduled_email(job_id, scheduled_time)
        except Exception as e:
            # we don't raise this one, as email errors should not kill app
            print("Error sending scheduling email: " + str(e))
        
        return scheduled_time, job_id

    def get_observer_flash_info(self, test_config):
        """Take a test config object and construct flashing info for the observers.
        This returns a dict with node_id keys and values as dicts of the
        following structure:
        {
        'ip': IP ADDRESS OF OBSERVER,
        'image-ids': ARRAY OF IMAGE IDS TO FLASH
        }
        """
        # gather for each observer:
        # - IP address
        # - list of image ids to send (i.e. images needed by its motes)
        observer_info = {}
        for image_device in test_config['image-devices']:
            image_id = image_device['image-id']

            for mote_id in image_device['mote-ids']:
                node_id = self.node_register.get_node_id_for_mote(mote_id)
                if node_id in observer_info.keys():
                    if image_id not in observer_info[node_id]['image-ids']:
                        observer_info[node_id]['image-ids'].append(image_id)
                else:
                    observer_info[node_id] = {
                        'ip': self.node_register.get_node_ip(node_id),
                        'image-ids': [image_id]
                    }

        return observer_info

    def start_job(self, job_id, start_time):
        """
        Actually begin a job when its scheduled time comes

        Args:
            job_id (int): job ID to pass to observers
            start_time (datetime.datetime): test start time
        
        Raises:
            FileNotFoundError: firmware not found
        """
        try:
            self.influxdb.insert_server_log(job_id,
                                            f'Gathering info for test run...')

            test_id = self.mysql.get_test_id(job_id)
            if test_id is None:
                print("Job has been deleted. Ignoring...")

            config_path = uploader.get_config_path(self.upload_path, test_id)
            parsed_config = uploader.get_config_data(self.upload_path, test_id)

            duration_seconds = parsed_config['duration']

            # Clear when starting new job
            self.current_job = {
                "job_id": job_id,
                "test_id": test_id,
                "duration": duration_seconds,
                "start_time": start_time,
            }

            # Ensure that iot_actions is removed from an old test
            self.mqtt_client.set_iot_actions(None)

            print('This is the parsed config:' + str(parsed_config))
            if 'iot-controller-id' in parsed_config:
                # tell MQTT_Client to load iot listener
                iot_controller_id = parsed_config['iot-controller-id']
                iot_controller = import_module('uploads.iot_controllers.' +
                                               str(iot_controller_id))

                iot_actions = iot_controller.iot_actions

                self.mqtt_client.set_iot_actions(iot_actions)

                del iot_actions

            # check images are uploaded
            required_ids = uploader.get_config_image_ids(
                self.upload_path,
                test_id
            )
            success, remaining = self.mysql.check_images_uploaded(required_ids)
            if not success:
                raise Exception(
                    f'Some image IDs are missing for test {test_id}: '
                    f'{required_ids}'
                )

            # gather image paths ready for SCP and check they're present
            image_info = {}
            for image_id in required_ids:
                img_path = None
                img_data = self.mysql.get_image_data(image_id)
                img_type = img_data['type']
                img_os = img_data['os']
                for file in glob.glob(
                        f"{self.upload_path}/images/{image_id}-{img_os}-{img_type}.*"
                ):
                    img_path = file
                if not os.path.exists(img_path):
                    raise FileNotFoundError(
                        errno.ENOENT,
                        'Missing image file {img_path}!'
                    )

                image_info[image_id] = {
                    'path': img_path,
                    'type': img_type,
                    'os': img_os
                }

            # gather observer info ready for sending
            observer_info = self.get_observer_flash_info(parsed_config)

            motes = self.get_motes_for_config(parsed_config)

            self.influxdb.insert_server_log(job_id,
                                            'Uploading images to observers...')

            # send images to observers
            uploader.send_observer_images(
                observer_info,
                image_info,
                test_id,
                config_path,
                self.node_register.get_serial_id(),
                self.pi_password
            )
            # record start time
            self.influxdb.insert_server_log(job_id,
                                            f'START TEST {test_id}')

            # send mqtt message to observers to initiate flash/test run
            self.mqtt_client.send_start_signal(test_id, job_id)
            self.node_register.update_mote_status('start', motes)

        except Exception as e:
            print(f'Error when starting job: {e}')
            self.mysql.mark_job_completed(job_id)
            self.email_client.send_error_email(job_id, str(e))

            raise e

    def stop_timer(self, finish_flash_time, failed):
        """
        Schedule the end of a test

        Args:
            finish_flash_time (datetime.datetime): time that mote flashing finished
            failed (list(int)): motes that failed to flash
        """
        def stop_callback(test_id, job_id, duration_seconds, start_time):
            """
            Callback to end tests once their duration has passed

            Args:
                test_id (int): test ID for job
                job_id (int): job ID to stop
                duration_seconds (int): duration of test
                start_time (datetime.datetime): start time of test
            """
            # send kill message
            self.mqtt_client.send_stop_signal(test_id)
            # mark as completed
            self.mysql.mark_job_completed(job_id)
            # send completion notification
            # self.email_client.send_success_email(job_id)
            # Job end time is calculated from once the flashing is complete 
            # plus duration
            end_time = finish_flash_time + timedelta(seconds=duration_seconds)
            print('Start time: ' + str(start_time))
            print('End time: ' + str(end_time))
            self.influxdb.insert_iot_logs(
                iot_logging.get_iot_logs(job_id, start_time, end_time)
            )

            # Update status of relevant motes to be ending test
            config_path = uploader.get_config_path(self.upload_path, test_id)
            parsed_config = uploader.get_config_data(self.upload_path, test_id)
            motes = self.get_motes_for_config(parsed_config)
            self.node_register.update_mote_status('end', motes)

            # Run default firmware everywhere if nothing further is scheduled
            scheduled_jobs = self.mysql.get_all_jobs(False)
            if len(scheduled_jobs) == 0:
                self.mqtt_client.send_default_run_signal()

        print("STOP SIGNAL TIMER GOING OFF - ALL MOTES FLASHED")

        job_id = self.current_job["job_id"]
        duration_seconds = self.current_job["duration"]

        # Check for flashing failure and cut short test if any did fail
        if len(failed) != 0:
            self.influxdb.insert_server_log(job_id,
                f'Error flashing mote {failed}, ensure firmware is compiled correctly. Ending test')
            duration_seconds = 3
        else:
            self.influxdb.insert_server_log(job_id,
                f'All motes have been flashed, commencing test duration of {duration_seconds} seconds')

        # stop executing after duration has elapsed
        stop_timer = Timer(float(duration_seconds),
                    lambda: stop_callback(self.current_job["test_id"], 
                                          job_id,
                                          duration_seconds,
                                          self.current_job["start_time"],))


        # wait for test duration
        stop_timer.start()


    def get_current_time(self):
        """
        Returns the current time formatted for the database.
        """
        dt = datetime.now()
        return dt

    def calculate_next_slot(self, test_id, start_time):
        """
        Calculates the next avaliable slot using the test_id and start_time of that test. Finds the
        duration of the test using its test_id then adds this duration to the start time as well as
        the padding specified in 'server.py'.
        """
        (_, _, duration) = self.mysql.get_test_info(test_id)
        return start_time + timedelta(seconds=duration) \
            + timedelta(seconds=self.padding*2)

    def get_slot(self, test_id):
        """
        Will return the next avaliable slot that the scheduler is keeping track of. If the slot is in
        the past, the test will be scheduled to run in 30 seconds time. Otherwise, the calculated slot
        is returned.
        """
        current_time = self.get_current_time()

        if self.next_slot > current_time:
            # Next slot is in the future, return that slot
            slot = self.next_slot
        else:
            # Next slot is in the past, return current time plus small buffer defined by 'buffer'
            slot = current_time + timedelta(seconds=self.buffer)

        self.next_slot = self.calculate_next_slot(test_id, slot)

        return slot

    def reschedule_incomplete_tests_on_startup(self):
        """
        Upon server initialisation, will reschedule tests in the database that were incomplete. This is
        such that if the server crashses, incompleted tests will be rescheduled. 
        """
        jobs = self.mysql.get_all_jobs(False)

        for job in jobs:

            job_id = job['job_id']
            test_config_id = job['test_id']

            self.mysql.delete_job(job_id)

            self.create_job(test_config_id)

        return len(jobs)

    def schedule_default_firmware(self):
        """
        Add some time to the next slot to allow for default firmware flashing
        """
        self.next_slot += timedelta(seconds=self.buffer)

    def get_motes_for_config(self, config):
        """
        Get a list of mote IDs given a config dict

        Args:
            config (dict): parsed test configuration
        
        Returns:
            motes (list(int)): motes in config
        """
        motes = []
        for image_device in config['image-devices']:
            motes.extend(image_device['mote-ids'])
        return motes
