from flask import request, make_response
from flask import current_app as app
from flask import Blueprint
import datetime

test_routes = Blueprint('test_routes', __name__)


@test_routes.route('/test-start-job', methods=['GET'])
def test_route_start_job():
    job_id = None
    if 'job_id' in request.args:
        job_id = request.args.get('job_id')
    elif 'test_id' in request.args:
        test_id = request.args.get('test_id')
        time = datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%f")
        job_id = app.mysql_db.create_job(test_id, time)
    else:
        return make_response('Please provide either test_id or job_id.',
                             400)

    app.scheduler.start_job(job_id)

    return make_response(f'Started job {job_id}.', 200)


@test_routes.route('/test-log-point', methods=['GET'])
def test_route_log_point():
    for key in ['job_id', 'node_id', 'device_id',
                'level', 'message']:
        if key not in request.args:
            return make_response(f'Missing key {key} in request params.', 400)

    node_id = request.args.get('node_id')
    device_id = request.args.get('device_id')
    level = request.args.get('level')
    message = request.args.get('message')
    job_id = request.args.get('job_id')

    time = datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%f")

    app.influx_db.insert_log(job_id,
                             node_id,
                             device_id,
                             time,
                             level,
                             message)

    return make_response(f'Inserted log {message} successfully '
                         f'at time {time}.', 200)
