import pandas as pd
from mote_monitor import MoteMonitor


class NodeRegister:
    """
    Class for maintaining a record of registered observer nodes and sensor motes

    Attributes:
        register (dict): {observer_id: observer_ip}
        largest_node_id (int): largest recorded node ID
        mote_register (pandas.DataFrame): record of mote information
            (observer ID, mote ID, serial number, platform, port connected to)
        largest_mote_id (int): largest recorded mote ID
        mote_monitor (mote_monitor.MoteMonitor): record of mote statuses
    """
    def __init__(self, config):
        self.register = {}
        self.largest_node_id = 0
        self.mote_register = pd.DataFrame(columns=["node_id",
                                                   "mote_id",
                                                   "serial",
                                                   "platform",
                                                   "interface"])
        self.largest_mote_id = 0
        self.mote_monitor = MoteMonitor(config['cca-rolling-average-size'], config['noise-floor-rolling-average-size'])

    def get_new_node_id(self, proposed_id, ip):
        """
        Assign an ID for an observer node

        Args:
            proposed_id (int): an ID that an observer has stored in its ENV.yaml.
                This is checked to see if it is viable
            ip (str): IP address of observer node registering
        
        Returns:
            if this IP is already registered with that ID
                return proposed ID 
            else if IP is registered with another ID:
                return that ID
            else
                return a new ID, largest ID + 1
        """
        # Valid entry in register, return proposed ID
        if str(proposed_id) in self.register.keys() and self.register[str(proposed_id)] == ip:
            if int(proposed_id) > self.largest_node_id:
                self.largest_node_id = int(proposed_id)
            return proposed_id

        # Search for IP:
        # if found with wrong ID, return the correct ID
        # if not found, return a new ID
        for node_id in self.register.keys():
            if self.register[node_id] == ip:
                if int(node_id) > self.largest_node_id:
                    self.largest_node_id = int(node_id)
                return node_id

        self.largest_node_id += 1
        return self.largest_node_id

    def register_node(self, node_id, node_ip):
        """
        Add an observer node to the node register

        Args:
            node_id (int): ID of observer node
            node_ip (str): IP address of observer node
        """
        print(f"Registering node, ID: {node_id}, IP: {node_ip}")
        if node_id in self.register and self.register[node_id] != node_ip:
            print("Node ID already in register, that shouldn't happen. "
                  "Check noderegister.get_new_node_id(...)")
        self.register[node_id] = node_ip

        print(self.register)

    def get_node_ip(self, node_id):
        """
        Get the IP address for an observer node ID

        Args:
            node_id (int): node ID to get IP address for
        
        Raises:
            KeyError: if node ID not found
        """
        try:
            return self.register[node_id]
        except KeyError as e:
            print(f'ERROR: Node with id {node_id} is not registered!')
            raise e

    def get_registered(self):
        """Wrapper getter function for observer node register
        """
        return self.register

    def register_motes(self, node_id, motelist):
        """
        Add motes to the mote register

        Args:
            node_id (int): observer node ID
            motelist (dict): mapping of mote serial numbers to ports
        """
        # Get any motes currently registered for this node
        existing_motes = self.mote_register.loc[
            self.mote_register['node_id'] == node_id
        ]
        for index, mote in zip(list(existing_motes.index),
                               list(existing_motes['serial'])):
            # Remove if disconnected (i.e. not in the list of motes received)
            if mote not in motelist.keys():
                print("Mote ", mote, " is no longer connected")
                self.mote_monitor.del_motes([mote])
                self.mote_register.drop(index, inplace=True)

        for key in motelist.keys():
            # Get any existing entries for this mote serial number
            curr = self.mote_register.loc[self.mote_register['serial'] == key.replace("'", "")]
            dropped = False
            # Check if this entry is accurate in terms of
            # interface and mote ID, delete if not
            if ((not curr.empty) and
                (curr['node_id'].iloc[0] != node_id or
                 curr['interface'].iloc[0] != motelist[key])):
                self.mote_monitor.del_motes([str(key)])
                self.mote_register.drop(curr.index[0], inplace=True)
                dropped = True

            # If we deleted an inaccurate entry or it was never there, add it
            if (curr.empty or dropped):
                self.mote_register = self.mote_register.append({
                    "node_id": node_id,
                    "mote_id": self.largest_mote_id + 1,
                    "serial": str(key).replace("'", ""),
                    "platform": self.serial_to_board_type_short(str(key).replace("'", "")),
                    "interface": motelist[key]
                }, ignore_index=True)
                self.largest_mote_id += 1
                self.mote_monitor.add_motes({self.largest_mote_id: str(key)}, node_id)
        print('Motes registered, register updated:')
        print(self.mote_register.serial)

    def get_registered_motes(self):
        """Getter for mote register
        """
        return self.mote_register

    def get_node_id_for_mote(self, mote_id):
        """
        Retrieve the observer that a mote is connected to

        Args:
            mote_id (int): ID of mote to find
        
        Returns:
            node_id (int): ID of observer that the mote is connected to
        
        Raises:
            Exception: mote ID not found
        """
        motes = self.mote_register.loc[
            self.mote_register['mote_id'] == mote_id
        ]

        if motes.empty:
            raise Exception(f'Mote {mote_id} is not currently registered!')

        return motes['node_id'].iloc[0]

    def get_serial_id(self):
        """
        Get the serial numbers for each mote

        Returns:
            dict: {mote_id: serial_number}
        """
        # Replaces single quotes which get added to Pandas
        removed_quotes = [w.replace("'", '')
                          for w
                          in self.mote_register.serial.tolist()]
        return dict(zip(self.mote_register.mote_id.tolist(), removed_quotes))
    
    def get_mote_platform(self):
        """
        Get the platform for each mote

        Returns:
            dict: {mote_id: platform}
        """
        return dict(zip(self.mote_register.mote_id.tolist(),
                        self.mote_register.platform.tolist()))

    def get_registered_motes_dict(self):
        """
        Get a mapping from mote ID to sensor mote type

        Returns:
            dict: {mote_id: sensor_mote_type (friendly format)}
        """
        cols = self.mote_register[['mote_id', 'serial']]

        record_list = cols.to_dict('records')

        for record in record_list:
            board_type = self.serial_to_board_type(record['serial'])
            record['board_type'] = board_type
            del record['serial']

        return record_list

    def serial_to_board_type(self, serial):
        """
        Get the sensor mote (friendly format) type given the serial number

        Args:
            serial (str): serial number to convert to mote type
        """
        # NOTE: this will need to be updated to account for more mote types
        if serial.startswith('ZOL'):
            return "Zolertia RE-Mote rev-b"
        else:
            return "TelosB"

    def serial_to_board_type_short(self, serial):
        """
        Get the sensor mote (short format) type given the serial number

        Args:
            serial (str): serial number to convert to mote type
        """
        # NOTE: this will need to be updated to account for more mote types
        if serial.startswith('ZOL'):
            return "zolertia"
        else:
            return "telosb"

    def update_motes_all_idle(self):
        """
        Wrapper function for updating the status of every mote in the mote 
        monitor to be idle
        """
        self.mote_monitor.update_status_idle_all()

    def update_motes_node_idle(self, node_id):
        """
        Wrapper function for updating the status of all motes connected to 
        the specified observer node to idle

        Args:
            node_id (int): ID of observer to set all motes connected to it 
                to be idle
        """
        self.mote_monitor.update_status_idle_node(node_id)

    def update_mote_status(self, event, serial):
        """
        Wrapper function for updating the status of a mote based on an event

        Args:
            event (str): can be 'start', 'end' (in relation to a test) or 'idle'
            serial (str): serial number of mote to update status of
        """
        if event == 'idle':
            self.mote_monitor.update_status_idle(serial)
        elif event == 'start':
            self.mote_monitor.update_status_start(serial)
        elif event == 'end':
            self.mote_monitor.update_status_end(serial)

    def get_mote_status(self):
        """
        Get the status of every connected mote

        Returns:
            dict of dicts:
                {
                    mote_id: {
                        platform: ,
                        status: 
                    }
                }
        """
        status = self.mote_monitor.get_status_all()
        serial = self.get_serial_id()
        res = {}
        for k in status.keys():
            res[k] = {
                'platform': self.serial_to_board_type(serial[k]),
                'status': status[k]
            }

        return res
    
    def check_motes_registered(self, motes):
        """
        Check to see if all of the motes given are registered (for a configuration file)

        Args:
            motes (list(int)): mote IDs to check for
        
        Returns:
            True, [] if all motes registered
                else False, [unregistered_mote_ids]
        """
        unregistered = []

        for mote_id in motes:
            if mote_id not in self.mote_register.mote_id.values:
                unregistered.append(mote_id)
        
        return not bool(unregistered), unregistered
