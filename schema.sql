CREATE TABLE users (
  user_id INT AUTO_INCREMENT PRIMARY KEY,
  user_email VARCHAR(100) NOT NULL,
  user_password_hash_salt CHAR(77) NOT NULL,
  user_username VARCHAR(100) NOT NULL
);

CREATE TABLE test_configurations (
  test_id INT AUTO_INCREMENT PRIMARY KEY,
  user_id INT NOT NULL,
  test_checksum CHAR(40) NOT NULL,
  date_uploaded DATETIME NOT NULL,
  test_name VARCHAR(50) NOT NULL,
  test_description VARCHAR(200) NOT NULL,
  duration INT NOT NULL
);

CREATE TABLE images (
  image_id INT AUTO_INCREMENT PRIMARY KEY,
  user_id INT NOT NULL,
  image_checksum CHAR(40) NOT NULL,
  image_type VARCHAR(10) NOT NULL,
  date_uploaded DATETIME NOT NULL,
  date_last_used DATETIME NOT NULL,
  image_name VARCHAR(50) NOT NULL,
  image_description VARCHAR(200) NOT NULL,
  image_os VARCHAR(10) NOT NULL
);

CREATE TABLE jobs (
  job_id INT AUTO_INCREMENT PRIMARY KEY,
  test_id INT NOT NULL,
  scheduled_start_time DATETIME NOT NULL,
  completed BIT(1) NOT NULL
);

CREATE TABLE iot_controllers (
  iot_controller_id INT AUTO_INCREMENT PRIMARY KEY,
  user_id INT NOT NULL,
  iot_controller_checksum CHAR(40) NOT NULL,
  iot_controller_name VARCHAR(50) NOT NULL,
  iot_controller_description VARCHAR(200) NOT NULL,
  date_uploaded DATETIME NOT NULL,
  date_last_used DATETIME NOT NULL
);
