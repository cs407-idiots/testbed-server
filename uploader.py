import os
import yaml
import errno
from fabric import Connection


def get_config_raw(upload_path, test_id):
    """Read in config as string"""
    folder_path = os.path.join(upload_path, 'tests')

    for filename in os.listdir(folder_path):
        if filename == str(test_id) + '.yaml':
            with open(os.path.join(folder_path, filename), 'r') as f:
                return f.read()
    else:
        raise FileNotFoundError(
            errno.ENOENT,
            f'Configuration {test_id} has not been uploaded yet.')


def get_config_path(upload_path, test_id):
    """Build config path for a test ID"""
    folder_path = os.path.join(upload_path, 'tests', f'{test_id}.yaml')

    return folder_path


def get_config_data(upload_path, test_id):
    """Get the parsed config YAML for the given test configuration."""
    config_raw = get_config_raw(upload_path, test_id)
    return yaml.safe_load(config_raw)


def get_config_image_ids(upload_path, config_id):
    """Get image IDs listed in a configuration"""
    config = get_config_data(upload_path, config_id)

    print(config['image-ids'])
    return config['image-ids']


def create_upload_folders(upload_path):
    """Create folders for upload of files from the frontend"""
    if not os.path.exists(upload_path):
        os.mkdir(upload_path)

    images_path = os.path.join(upload_path, 'images')
    if not os.path.exists(images_path):
        os.mkdir(images_path)

    tests_path = os.path.join(upload_path, 'tests')
    if not os.path.exists(tests_path):
        os.mkdir(tests_path)

    iot_path = os.path.join(upload_path, 'iot_controllers')
    if not os.path.exists(iot_path):
        os.mkdir(iot_path)


# NOTE: unused and behaviour cannot be guaranteed
def check_config_images(upload_path, config_name):
    """
    Check all required images are present for a given configuration

    NOTE: UNUSED AND BEHAVIOUR IS UNTESTED
    """
    folder_path = os.path.join(upload_path, config_name)
    if not os.path.isdir(folder_path):
        return False, {
            "valid": False,
            "error": f"Configuration {config_name} has not been uploaded yet."
        }

    config = None
    try:
        config = get_config_data(upload_path, config_name)
    except FileNotFoundError:
        return False, {
            "valid": False,
            "error": f"Configuration {config_name} has not been uploaded yet."
        }

    # get missing files for helpful errors
    missing_images = []
    for image in config['images']:
        if not os.path.isfile(os.path.join(folder_path, image['image_path'])):
            missing_images.append(image['image_path'])
    if missing_images:
        return False, {
            "valid": False,
            "error": "Image files are missing. See the missing_files "
            "list for details.",
            "missing_files": missing_images
        }

    return True, None


def send_observer_images(observer_info,
                         image_info,
                         test_id,
                         config_path,
                         id_serial,
                         pi_password):
    """
    Send images to observers via SCP.
    
    Arguments: 
        observer_info: a dict node id -> {
        'ip': ip,
        'image-ids': [image id]
        },
        image_info a dict image id -> {
        'path': image path,
        'type': image type
        }
        test_id
        config_path: path to test configuration file
        id_serial: dict mapping mote IDs to serial numbers
        pi_password: password for observer nodes
    
    Raises:
        FileNotFoundError: configuration not found
    """
    try:
        with open(config_path) as f:
            raw_config = f.read()
    except FileNotFoundError:
        print(f"Configuration for test {test_id} not found")
    config = yaml.safe_load(raw_config)

    # Maniuplation and transformation of configuration
    # Replace every mote ID with a dict:
    # {serial:, mote ID: }
    # So the observers can use it in logs
    for image_device in config['image-devices']:
        for i in range(len(image_device['mote-ids'])):
            image_device['mote-ids'][i] = {
                'serial': id_serial[image_device['mote-ids'][i]],
                'id': image_device['mote-ids'][i]
            }
            print(image_device['mote-ids'])

    # Write out temporarily
    with open(f'/tmp/{test_id}.yaml', 'w') as file:
        yaml.safe_dump(config, file)

    # SCP config to every observer node with motes being used
    # as well as every image they'll need
    for obs_id, obs_info in observer_info.items():
        with Connection(obs_info['ip'],
                        user='pi',
                        connect_kwargs={
                            'password': pi_password
                        }) as c:
            c.run('mkdir -p /home/pi/images')
            c.run('mkdir -p /home/pi/tests')
            for image_id in obs_info['image-ids']:
                print("image_id: " + str(image_id))
                print("image_info: " + str(image_info))
                image_path = image_info[image_id]['path']
                # format = bin/hex
                image_format = image_path.rsplit('.', 1)[1].lower()
                image_type = image_info[image_id]['type']
                image_os = image_info[image_id]['os']
                c.put(local=image_path,
                      remote=(
                          '/home/pi/images/'
                          f'{image_id}-{image_os}-{image_type}.{image_format}'
                      ))
            c.put(local=f'/tmp/{test_id}.yaml',
                  remote=f'/home/pi/tests/{test_id}.yaml')
