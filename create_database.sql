DROP DATABASE IF EXISTS lucidlab;
CREATE DATABASE lucidlab DEFAULT CHARACTER SET utf8mb4 DEFAULT COLLATE utf8mb4_unicode_ci;
DROP USER 'lucidlab'@'localhost';
FLUSH PRIVILEGES;
CREATE USER 'lucidlab'@'localhost' IDENTIFIED BY 'lucidlab';
GRANT ALL PRIVILEGES ON lucidlab.* TO 'lucidlab'@'localhost';
FLUSH PRIVILEGES;
