import pandas as pd
from datetime import datetime as dt

class MoteMonitor:
    """
    Class to maintain a record of mote status, last contact, and network metrics

    Attributes:
        mote_status (pandas.DataFrame): storage of mote details (ID, serial number, 
            observer ID, status, last contact)
        mote_addr_to_id_dict (dict): mapping of mote addresses (used in 802.15.4 communication)
        rssi_dict (dict of dicts): mapping of mote IDs to a dict mapping other mote IDs to RSSI 
            values
        cca_dict (dict): mapping of mote ID to list of CCA results for the 16 2.4GHz channels
        cca_rolling_average (int): number of values to calculate CCA averages over
    """
    def __init__(self, cca_rolling_average, noise_floor_rolling_average):
        self.mote_status = pd.DataFrame(columns=['mote_id',
                                            'serial',
                                            'node_id',
                                            'status',
                                            'last_contact'])
        self.mote_addr_to_id_dict = {}
        self.rssi_dict = {}
        self.cca_dict = {}
        self.cca_rolling_average = cca_rolling_average
        self.noise_floor_dict = {}
        self.noise_floor_rolling_average = noise_floor_rolling_average

    def add_motes(self, motes, node_id):
        """
        Add motes to the mote monitor

        Args:
            motes (dict): mapping of mote IDs to serial numbers
            node_id (int): observer node ID
        """
        for mote in motes.keys():
            # Remove existing entries for this mote ID and serial number
            self.mote_status = self.mote_status[self.mote_status.mote_id != mote]
            self.mote_status = self.mote_status[self.mote_status.serial != motes[mote].replace("'", "")]
            
            self.mote_status = self.mote_status.append({'mote_id': mote,
                                                        'serial': motes[mote].replace("'", ""),
                                                        'node_id': node_id,
                                                        'status': 'Initialising',
                                                        'last_contact': dt.now()},
                                                        ignore_index=True)
            print(f"Mote ID {mote} added to mote monitor")

    def del_motes(self, motes):
        """
        Remove motes from the mote monitor (on disconnection)

        Args:
            motes (list(str)): list of serial numbers to remove
        """
        motes_stripped = [i.replace("'", "") for i in motes]
        for mote in motes_stripped:
            self.mote_status = self.mote_status[self.mote_status.serial != mote]
        print(f"Mote serial {mote} removed from mote monitor")
    
    def get_status_all(self):
        """
        Get the status of every mote in the mote monitor

        Returns:
            dict: mapping mote ID to mote status
        """
        if len(self.mote_status) == 0:
            return {}
        self.update_status_all()
        # Replaces single quotes just in case
        return dict(zip(self.mote_status.mote_id.tolist(), [w.replace("'", '') for w in self.mote_status.status.tolist()]))

    def update_status_all(self):
        """
        Refresh the mote monitor to check for timeout
        """
        # Refresh for timeout
        # Any mote that isn't running a test and hasn't produced an output in
        # over 5 minutes is considered disconnected
        if self.mote_status.empty:
            return
        self.mote_status.loc[
            ((dt.now() - self.mote_status.last_contact).dt.total_seconds() > 300) &
            ((self.mote_status.status == 'Idle') | (self.mote_status.status == 'Ending test')),
            'status'] = 'Connection timeout'
        print("Timeout check complete")

    def update_status_idle(self, mote):
        """
        Update the status of an idle mote by updating its last contact to now. Set 
        its status to be idle if it is not currently 'running test' (this may be the case 
        if it sends an idle signal whilst preparing to start a test)

        Args:
            mote (str): serial number of mote to update
        """
        self.mote_status.loc[self.mote_status.serial == mote, 'last_contact'] = dt.now()
        self.mote_status.loc[(self.mote_status.serial == mote) & (self.mote_status.status != 'Running test'), 'status'] = 'Idle'
        self.update_status_all()
        print(f"Idle signal received from mote serial {mote}")
    
    def update_status_idle_all(self):
        """
        Set the status of every mote to idle
        """
        self.mote_status.status = 'Idle'
    
    def update_status_idle_node(self, node_id):
        """
        Set the status of every mote connected to the specified observer to idle

        Args:
            node_id (int): node ID to set motes to idle
        """
        self.mote_status.loc[self.mote_status.node_id == node_id, 'last_contact'] = dt.now()
        self.mote_status.loc[self.mote_status.node_id == node_id, 'status'] = 'Idle'
        self.update_status_all()
    
    def update_status_start(self, motes):
        """
        Update the status of the specified motes to be 'Running test'

        Args:
            motes (list(int)): list of mote IDs to update status of
        """
        self.mote_status.loc[self.mote_status.mote_id.isin(motes), 'status'] = 'Running test'
        self.update_status_all()
        print("Start signal detected for mote IDs", motes)
    
    def update_status_end(self, motes):
        """
        Update the status of the specified motes to be 'Ending test'

        Args:
            motes (list(int)): list of mote IDs to update status of
        """
        self.mote_status.loc[self.mote_status.mote_id.isin(motes), 'status'] = 'Ending test'
        self.update_status_all()
        print("End signal detected for mote IDs ", motes)

    def register_in_rssi_dict(self, device_serial):
        """
        Register a mote to store RSSI values with other motes

        Args:
            device_serial (str): serial number of mote to register
        """
        mote_id = self.serial_to_mote(device_serial)

        if mote_id not in self.rssi_dict.keys():
            self.rssi_dict[mote_id] = {
                "status": "",
                "RSSIs": {}
            }

    def register_in_cca_dict(self, device_serial):
        """
        Register a mote to store results of CCA

        Args:
            device_serial (str): serial number of mote to register
        """
        mote_id = self.serial_to_mote(device_serial)
        
        if mote_id not in self.cca_dict.keys():
            self.cca_dict[mote_id] = self.cca_rolling_average * [None]


    def register_in_noise_floor_dict(self, device_serial):
        """
        Register a mote to store results of noise floor

        Args:
            device_serial (str): serial number of mote to register
        """
        mote_id = self.serial_to_mote(device_serial)
        
        if mote_id not in self.noise_floor_dict.keys():
            self.noise_floor_dict[mote_id] = self.noise_floor_rolling_average * [None]


    def update_rssi_dict(self, device_serial, mote_addr, src_addr, rssi):
        """
        Update an RSSI value between motes

        Args:
            device_serial (str): serial number of mote to register
            mote_addr (str): address of mote receiving broadcast
            src_addr (str): address of mote receiving broadcast
            rssi (str): RSSI value from broadcast
        """
        # Get the mote ID from the serial number
        mote_id = self.serial_to_mote(device_serial)

        # Register the address of the mote if not already registered
        if mote_addr not in self.mote_addr_to_id_dict.keys():
            self.mote_addr_to_id_dict[mote_addr] = mote_id

        # Store the RSSI value from the broadcast where all addresses are present and identifiable
        if src_addr in self.mote_addr_to_id_dict.keys():
            src_mote_id = self.mote_addr_to_id_dict[src_addr]
            self.rssi_dict[mote_id]["status"] = self.mote_status[self.mote_status['mote_id'] == mote_id]['status'].values[0]
            self.rssi_dict[mote_id]["RSSIs"][src_mote_id] = int(rssi)

    def get_rssi_dict(self):
        """
        Getter for RSSI values dict

        Returns:
            rssi_dict (dict): values of RSSI between motes
        """
        for mote_id in self.rssi_dict.keys():
            self.rssi_dict[mote_id]["status"] = self.mote_status[self.mote_status['mote_id'] == mote_id]['status'].values[0]
        return self.rssi_dict

    def serial_to_mote(self, serial):
        """
        Get mote ID for serial number

        Args:
            serial (str): serial number of mote

        Returns:
            int: mote ID
        """
        return self.mote_status['mote_id'].loc[self.mote_status['serial']==serial].values[0]

    def set_cca_dict(self, device_serial, cca_dict):
        """
        Add received CCA values for a mote to the dict and pop the oldest values

        Args:
            device_serial (str): serial number of mote to update
            cca_dict (dict): new values to add to CCA dict
        """
        mote_id = self.serial_to_mote(device_serial)

        self.cca_dict[mote_id].append(self.cca_dict[mote_id].pop(0))
        self.cca_dict[mote_id][-1] = cca_dict

    def get_cca_mote_averages(self, mote_id):
        """
        Calculate the average CCA values across each channel for a mote

        Args:
            mote_id (int): mote ID of averages to retrieve
        
        Returns:
            averages (list(int)): rolling average of CCA values for each channel
        """
        averages = []

        if mote_id not in self.cca_dict.keys() or self.cca_dict[mote_id].count(None) == self.cca_rolling_average:
            return []

        for i in range(16): #16 channels from the CCA metrics
            channel_total = 0

            for j in range(self.cca_rolling_average):
                if self.cca_dict[mote_id][j]:
                    channel_total += self.cca_dict[mote_id][j][i]

            average_divisor = self.cca_rolling_average - self.cca_dict[mote_id].count(None)
            averages.append(channel_total / average_divisor)
            
        return averages

    def get_cca_global_averages(self):
        """
        Calculate average CCA results over every mote

        Returns:
            global_averages (list(int)): global rolling averages of CCA across every mote
        """
        global_averages = []
        if len(self.cca_dict.keys()) == 0:
            return []

        mote_aves = []
        
        for mote_id in self.cca_dict.keys():
            temp_aves = self.get_cca_mote_averages(mote_id)
            if not len(temp_aves) == 0:
                mote_aves.append(temp_aves)

        global_averages = list(map(sum, zip(*mote_aves)))
        global_averages = list(map(lambda l : l / len(mote_aves), global_averages))

        return global_averages

    def set_noise_floor_dict(self, device_serial, noise_floor_dict):
        """
        Add received noise floor values for a mote to the dict and pop the oldest values

        Args:
            device_serial (str): serial number of mote to update
            noise_floor_dict (dict): new values to add to noise floor dict
        """
        mote_id = self.serial_to_mote(device_serial)

        self.noise_floor_dict[mote_id].append(self.noise_floor_dict[mote_id].pop(0))
        self.noise_floor_dict[mote_id][-1] = noise_floor_dict
    
    
    def get_noise_floor_mote_averages(self, mote_id):
        """
        Calculate the average noise floor values across each channel for a mote

        Args:
            mote_id (int): mote ID of averages to retrieve
        
        Returns:
            averages (list(int)): rolling average of noise floor values for each channel
        """
        averages = []

        if mote_id not in self.noise_floor_dict.keys() or self.noise_floor_dict[mote_id].count(None) == self.noise_floor_rolling_average:
            return []

        for i in range(16): #16 channels from the noise floor metrics
            channel_total = 0

            for j in range(self.noise_floor_rolling_average):
                if self.noise_floor_dict[mote_id][j]:
                    channel_total += self.noise_floor_dict[mote_id][j][i]

            average_divisor = self.noise_floor_rolling_average - self.noise_floor_dict[mote_id].count(None)
            averages.append(channel_total / average_divisor)
            
        return averages

    def get_noise_floor_global_averages(self):
        """
        Calculate average noise floor results over every mote

        Returns:
            global_averages (list(int)): global rolling averages of noise floor across every mote
        """
        global_averages = []
        if len(self.noise_floor_dict.keys()) == 0:
            return []

        mote_aves = []
        
        for mote_id in self.noise_floor_dict.keys():
            temp_aves = self.get_noise_floor_mote_averages(mote_id)
            if not len(temp_aves) == 0:
                mote_aves.append(temp_aves)

        global_averages = list(map(sum, zip(*mote_aves)))
        global_averages = list(map(lambda l : l / len(mote_aves), global_averages))

        return global_averages
