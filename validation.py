def allowed_config_filename(filename):
    """
    Check file extension to ensure configuration file ends in 'yaml'

    Args:
        filename (str): filename to check

    Returns:
        True if valid
            else False
    """
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() == 'yaml'


def allowed_iot_controller_filename(filename):
    """
    Check file extension to ensure IoT contoller ends in 'py'

    Args:
        filename (str): filename to check

    Returns:
        True if valid
            else False
    """
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() == 'py'


def is_int(v):
    """
    Check if a value is an int

    Args:
        v (?): value to check
    
    Returns:
        True if int
            else False
    """
    try:
        int(v)
        return True
    except Exception:
        return False


def validate_yaml_format(sent_yaml):
    """
    Perform checks to ensure as much as possible that the user has uploaded 
    a valid configuration file on a semantic and syntactic level

    Args:
        sent_yaml (dict): parsed configuration
    
    Returns:
        True, None if valid
            else False, reason (str)
    """

    # Check for some basic required keys
    required_keys = ['duration',
                     'image-ids',
                     'image-devices']

    for key in required_keys:
        if key not in sent_yaml:
            return False, f'Missing key {key} in top-level config.'

    # Verify existence of IoT controller script if specified
    if 'iot-controller-id' in sent_yaml:
        if not is_int(sent_yaml['iot-controller-id']):
            return False, "IoT controller ID must be an integer"

    # Ensure duration is an integer
    if not is_int(sent_yaml['duration']):
        return False, 'Duration must be an integer.'

    # Ensure all image IDs are integers
    for image_id in sent_yaml['image-ids']:
        if not is_int(image_id):
            return False, f'image-ids contains non-int key {image_id}'

    # Perform checks on images->motes specification
    mentioned_motes = []
    for image_device in sent_yaml['image-devices']:
        # Check for required keys
        for key in ['image-id', 'mote-ids']:
            if key not in image_device:
                return False, ('An image-devices element '
                               f'is missing a key {key}')
            # Prevent specification of no motes for an image
            # This would likely be a mistake
            if len(image_device['mote-ids']) == 0:
                return False, ("No motes specified for "
                               f"image ID {image_device['image-id']}")
        # For each mote ID specified
        for mote_id in image_device['mote-ids']:
            # Ensure it's an integer
            if not is_int(mote_id):
                return False, ('mote-ids contains '
                                f'non-int key {mote_id}')
            else:
                # Check for duplicate mentions of motes
                if mote_id in mentioned_motes:
                    return False, ('Duplicate reference '
                                    f'to mote ID {mote_id}')
                else:
                    mentioned_motes.append(mote_id)

    return True, None
