from flask_mail import Message
from flask import current_app as app
import os
from string import Template


def open_template(template_filename):
    with open(template_filename, 'r') as f:
        return Template(f.read())


class EmailClient:
    def __init__(self, config, mail, mysql_db, app):
        self.config = config
        self.mysql_db = mysql_db
        self.mail = mail
        self.app = app

        self.success_template = open_template(os.path.join('email-templates',
                                                           'job-success.txt'))
        self.error_template = open_template(os.path.join('email-templates',
                                                         'job-error.txt'))
        self.scheduled_template = open_template(
            os.path.join('email-templates',
                         'job-scheduled.txt')
        )

    def send_success_email(self, job_id):
        try:
            test_info = self.mysql_db.get_job_info(job_id)
            email = test_info['user_email']

            message_text = self.success_template.safe_substitute(test_info)

            with self.app.app_context():
                msg = Message(
                    subject='LucidLab job completed',
                    sender=app.config.get('MAIL_USERNAME'),
                    recipients=[email],
                    body=message_text
                )
                self.mail.send(msg)
        except Exception as e:
            print('Exception sending email: ' + str(e))

    def send_error_email(self, job_id, error_message):
        try:
            test_info = self.mysql_db.get_job_info(job_id)
            email = test_info['user_email']
            test_info['error_message'] = error_message

            message_text = self.error_template.safe_substitute(test_info)

            with self.app.app_context():
                msg = Message(
                    subject='Error in LucidLab job',
                    sender=app.config.get('MAIL_USERNAME'),
                    recipients=[email],
                    body=message_text
                )
                self.mail.send(msg)
        except Exception as e:
            print('Exception sending email: ' + str(e))

    def send_scheduled_email(self, job_id, scheduled_time):
        try:
            test_info = self.mysql_db.get_job_info(job_id)
            email = test_info['user_email']
            test_info['scheduled_time_pretty'] = scheduled_time.strftime(
                '%A, %d %B, %Y, %H:%M'
            )

            message_text = self.scheduled_template.safe_substitute(test_info)

            with self.app.app_context():
                msg = Message(
                    subject='LucidLab job scheduled',
                    sender=app.config.get('MAIL_USERNAME'),
                    recipients=[email],
                    body=message_text
                )
                self.mail.send(msg)

            print("Scheduling email sent successfully.")
        except Exception as e:
            print('Exception sending email: ' + str(e))
