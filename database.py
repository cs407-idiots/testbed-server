from influxdb import InfluxDBClient
import mysql.connector
from mysql.connector import Error
from passlib.hash import sha256_crypt
import io
import csv
from datetime import datetime
import os

db_name = 'logging'


class InfluxDatabase:
    """
    Class to manage the InfluxDB instance with various wrapper functions

    Attributes:
        client (influxdb.InfluxDBClient): InfluxDB client
    """
    def __init__(self, config):
        self.client = InfluxDBClient(host=config['influxdb']['host'],
                                     port=config['influxdb']['port'],
                                     database=config['influxdb']['database'])

    def insert_server_log(self, job_id, message):
        """
        Add a log to the database regarding server operation

        Args:
            job_id (int): the job ID this log pertains to
            message (str): message to be inserted
        """
        self.insert_log(
            job_id,
            'SERVER',
            '',
            '',
            '',
            datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f"),
            'INFO',
            message,
            None,
            None,
            None,
            None
        )

    def insert_log(self,
                   job_id,
                   log_type,
                   node_id,
                   device_id,
                   mote_id,
                   time,
                   level,
                   message,
                   thing_id,
                   thing_name,
                   state_before,
                   state_after):
        """
        Write a log to InfluxDB

        Args:
            job_id (str): job ID the log pertains to
            log_type (str): type of log (IoT or sensor mote)
            node_id (str): observer node this log came from
            device_id (str): serial number of mote
            mote_id (str): server-assigned mote ID
            time (str): string representation of time of log
            level (str): for further categorisation
            message (str): log message
            thing_id (str): for IoT devices
            thing_name (str): friendly IoT device name
            state_before (str): IoT state before change according to openHAB
            state_after (str): IoT state after change according to openHAB
        """
        print(f'Inserting {log_type} log {message} for job {job_id},\n'
              f'time {time}, node_id {node_id}, device_id {device_id}, mote_id {mote_id}')

        self.client.write_points([{
            "measurement": "logging",
            "tags": {
                "job_id": job_id,
                "log_type": log_type,
                "node_id": node_id,
                "device_id": device_id,
                "mote_id": mote_id,
                "level": level
            },
            "time": time,
            "fields": {
                "message": message
            }
        }])

    def insert_iot_logs(self, log_array):
        """
        Simply calls write_points on the array of data points.
        Each point should be a dict containing the following structure:
        {
            "measurement": "logging",
            "tags": {
                "job_id": JOB_ID,
                "log_type": "iot",
                "thing_id": THING_ID,
                "thing_name": THING_NAME,
                "state_before": STATE_BEFORE,
                "state_after": STATE_AFTER
            },
            "time": TIMESTAMP
        }
        
        Args:
            log_array (list of dicts): list of logs to write
        """
        self.client.write_points(log_array)

    def close(self):
        """Close the Client
        """
        self.client.close()

    def retrieve_job_logs(self, job_id):
        """
        Retrieve the stored logs for the given job

        Args:
            job_id (int): job ID to retrieve logs for

        Returns:
            points (list): retrieved logs
        """
        job_id = str(job_id)

        # NOTE: influxdb requires single quotes for string args
        query = ("SELECT * "
                 f"FROM logging WHERE job_id='{job_id}';")

        results = self.client.query(query)
        points = list(results.get_points())

        return points

    def retrieve_job_logs_csv(self, job_id):
        """
        Retrieve the stored logs for the given job in CSV format

        Args:
            job_id (int): job ID to retrieve logs for

        Returns:
            str: logs in CSV format
        """
        job_id = str(job_id)
        query = ("SELECT * "
                 f"FROM logging WHERE job_id='{job_id}';")
        results = self.client.query(query)
        points = list(results.get_points())

        names = ['time', 'log_type', 'message', 'level', 'device_id', 'mote_id',
                 'node_id', 'thing_id', 'thing_name', 'state_before', 'state_after']

        sio = io.StringIO()
        csv_writer = csv.DictWriter(sio,
                                    fieldnames=names,
                                    extrasaction='ignore')

        csv_writer.writeheader()
        csv_writer.writerows(points)

        return sio.getvalue()

    def delete_job(self, job_id):
        """
        Delete logs for the given job

        Args:
            job_id (int): job ID to delete logs for
        """
        job_id = str(job_id)

        self.client.delete_series(
            measurement='logging',
            tags={
                'job_id': job_id
            }
        )


class MySQLDatabase:
    """
    Class for the management and interaction with the MySQL database instance

    Attributes:
        config (dict): contents of confi.yaml, contains MySQL server details
    """
    def __init__(self, config):
        try:
            self.connection = mysql.connector.connect(
                host=config['mysql']['host'],
                database=config['mysql']['database'],
                user=config['mysql']['user'],
                password=config['mysql']['password'],
                charset="utf8")
            self.padding=config['padding']

            if self.connection.is_connected():
                db_info = self.connection.get_server_info()
                print("Connected to server version ", db_info)

        except Error as e:
            print("Error connecting to MySQL: ", e)
            raise e

    def close(self):
        """Close MySQL connection
        """
        self.connection.close()
        print("MySQL connection closed.")

    def create_user(self, username, email, password):
        """
        Create a user and return their new ID
        
        Args:
            username (str): username
            email (str): email address
            password (str): password (would be sent via HTTPS in a real deployment)
        
        Raises:
            Error: on error with database insertion
        """

        hash_salt = sha256_crypt.hash(password)

        cursor = self.connection.cursor(prepared=True)
        query = """INSERT INTO users
        (user_email, user_username, user_password_hash_salt)
        VALUES (%s, %s, %s)"""

        try:
            cursor.execute(query, (email,
                                   username,
                                   hash_salt))
            self.connection.commit()

            id = cursor.lastrowid
            cursor.close()
            return id
        except Error as e:
            print("Error: " + str(e))
            raise e

    def query_user_login(self, username, password):
        """
        Attempt login for a user

        Args:
            username (str): username
            password (str): password (would be sent via HTTPS in a real deployment)
        
        Returns:
            True, user_id if successful
            else False, None
        
        Raises:
            Error: on failed login query
        """
        query = (
            "SELECT user_id, user_password_hash_salt "
            "FROM users "
            "WHERE user_username = %s "
            "LIMIT 1"
        )

        try:
            cursor = self.connection.cursor(prepared=True)
            cursor.execute(query.encode(), (username.encode('utf-8'),))

            results = cursor.fetchone()

            if results is None:
                return False, None
            (user_id, target_password_hash_salt) = results

            cursor.close()

            if sha256_crypt.verify(password, target_password_hash_salt):
                return True, user_id
            else:
                return False, None
        except Error as e:
            print('Error in login query: ' + str(e))
            raise e

    def create_configuration(self,
                             user_id,
                             checksum,
                             duration,
                             name,
                             description):
        """
        Create a record of a test configuration in the database

        Args:
            user_id (int): owner of this configuration
            checksum (str): configuration file checksum
            duration (int): duration of test in seconds
            name (str): test name
            description (str): test description
        """
        query = (
            "INSERT INTO test_configurations"
            "(user_id, test_checksum, "
            "test_name, test_description, duration, "
            "date_uploaded) VALUES"
            "(%s, %s, %s, %s, %s, NOW())"
        )

        cursor = self.connection.cursor(prepared=True)
        cursor.execute(query, (user_id,
                               checksum,
                               name,
                               description,
                               duration))
        self.connection.commit()

        id = cursor.lastrowid
        cursor.close()

        return id

    def delete_configuration(self, test_id):
        """
        Delete a record of a test configuration

        Args:
            test_id (int): test configuration ID to delete
        """
        query = (
            'DELETE FROM test_configurations '
            'WHERE test_id = %s'
        )

        cursor = self.connection.cursor(prepared=True)
        cursor.execute(query, (test_id,))
        self.connection.commit()

        cursor.close()

    def get_test_info(self, test_id):
        """
        Get information about a test ID for a run (name, description, duration)

        Args:
            test_id (int): test ID to gather information about
        
        Returns:
            Tuple: (test_name, test_description, duration) if exists
                else ('', '', 0)
        """
        query = (
            'SELECT test_name, test_description, duration '
            'FROM test_configurations '
            'WHERE test_id = %s '
            'LIMIT 1'
        )
        cursor = self.connection.cursor(prepared=True)
        cursor.execute(query, (test_id,))
        result = cursor.fetchone()

        if result:
            cursor.close()
            return result

        cursor.close()
        return ('', '', 0)  # Case where database is empty

    def create_image(self, user_id,
                     checksum,
                     image_type,
                     image_os,
                     image_name,
                     image_description):
        """
        Create a record of a firmware image

        Args:
            user_id (int): owner of the image
            checksum (str): image checksum
            image_type (str): platform of the image (zolertia or telosb)
            image_os (str): image OS (currently Contiki or RIOT)
            image_name (str): user-given image name
            image_description (str): user-given image description
        """
        query = (
            "INSERT INTO images"
            "(user_id, image_checksum, "
            "image_type, image_name, "
            "image_description, image_os, "
            "date_uploaded, date_last_used) VALUES"
            "(%s, %s, %s, %s, %s, %s, NOW(), NOW())"
        )

        cursor = self.connection.cursor(prepared=True)
        cursor.execute(query, (user_id, checksum,
                               image_type, image_name,
                               image_description, image_os))
        self.connection.commit()

        id = cursor.lastrowid
        cursor.close()
        return id

    def delete_image(self, image_id):
        """
        Delete the record of the given image

        Args:
            image_id (str): ID of image to have record deleted
        """
        query = (
            'DELETE FROM images '
            'WHERE image_id = %s'
        )

        cursor = self.connection.cursor(prepared=True)
        cursor.execute(query, (image_id,))

        self.connection.commit()
        cursor.close()

    def get_image_data(self, image_id):
        """
        Get image platform, OS, name, and description

        Args:
            image_id (int): image ID to get information for
        
        Returns:
            dict: {image_type:, os:, name:, description:}
        """
        query = (
            'SELECT image_type, image_os, '
            'image_name, image_description '
            'FROM images WHERE image_id=%s '
            'LIMIT 1'
        )

        cursor = self.connection.cursor(prepared=True)
        cursor.execute(query, (image_id,))

        (image_type, image_os,
         image_name, image_description) = cursor.fetchone()

        cursor.close()

        return {
            'type': image_type,
            'os': image_os,
            'name': image_name,
            'description': image_description
        }

    def check_images_uploaded(self, required_image_ids):
        """
        Given a list of image IDs, make sure they're all uploaded

        Args:
            required_image_ids (list(int)): image IDs to check for

        Returns:
            True, [] if all present
                else False, [remaining_image_IDs]
        """
        remaining = required_image_ids.copy()

        # every id must be present
        query = f'SELECT image_id FROM images;'
        cursor = self.connection.cursor()
        cursor.execute(query)

        for (id,) in cursor:
            # seen id, remove it
            while id in remaining:
                remaining.remove(id)

        cursor.close()
        # if there are any left over, they're missing
        return not bool(remaining), remaining
    
    def check_image_platform(self, image_motes, mote_platforms):
        """
        Given a dict of image IDs: mote IDs and mote IDs: platforms, 
        check if the platform of the mote matches the platform of the image

        Args:
            image_motes (dict: {image_id:[mote_ids]}): dict mapping image ID to a list
                of mote IDs as specified in a configuration file
            mote_platforms (dict: {mote_id: platform}): dict mapping mote ID to platform
        
        Returns:
            True, [] if platforms are consistent
                else False, [image_id, platform_of_image, mote_id, platform_of_mote]
                (for first instance of inconsistency)
        """
        # Select the image platforms from the database (only for those specified)
        format_string = ','.join(['%s'] * len(image_motes.keys()))
        query = (
            'SELECT image_id, image_type FROM '
            'images WHERE image_id IN (' + 
            format_string + ')'
        )
        cursor = self.connection.cursor(prepared=True)
        cursor.execute(query, tuple(image_motes.keys()))

        results = list(cursor.fetchall())

        # Build a dict of image ID to its platform
        image_platforms = {}
        for (image_id, platform) in results:
            image_platforms[image_id] = platform

        # For each image...
        for image_id in image_motes.keys():
            platform = image_platforms[image_id]
            
            # For each mote that is said to use that image in the configuration
            for mote_id in image_motes[image_id]:
                # Check if the platform of the mote and image match, return the inconsistency if not
                if platform != mote_platforms[mote_id]:
                    full_platform_image = ""
                    if platform == 'telosb':
                        full_platform_image = "TelosB"
                    else:
                        full_platform_image = "Zolertia RE-Mote rev-b"
                    full_platform_mote = ""
                    if mote_platforms[mote_id] == 'telosb':
                        full_platform_mote = "TelosB"
                    else:
                        full_platform_mote = "Zolertia RE-Mote rev-b"
                    return False, [image_id, full_platform_image, mote_id, full_platform_mote]
        return True, []


    def get_user_configurations(self, user_id):
        """
        Retrieve the test configurations specified by the user with the given ID

        Args:
            user_id (int): user ID to retrieve configurations for
        
        Returns:
            list of dicts:
                [
                    {
                        test_id: ,
                        name: ,
                        description: ,
                        duration: ,
                        date_uploaded:
                    },
                ]
        """
        query = (
            'SELECT test_id, test_name, duration, '
            'test_description, date_uploaded '
            'FROM test_configurations '
            'WHERE user_id = %s'
        )

        cursor = self.connection.cursor(prepared=True)
        cursor.execute(query, (user_id,))

        results = list(cursor.fetchall())

        results_dicts = [
            {
                'test_id': test_id,
                'name': name,
                'description': description,
                'duration': duration,
                'date_uploaded': date_uploaded
            } for (
                test_id,
                name,
                duration,
                description,
                date_uploaded
            ) in results
        ]

        ids = [row['test_id'] for row in results_dicts]

        cursor.close()
        return {
            'ids': ids,
            'test_configs': results_dicts
        }

    def get_user_images(self, user_id):
        """
        Retrieve the firmware images uploaded by the user with the given ID

        Args:
            user_id (int): user ID to retrieve images for
        
        Returns:
            list of dicts:
                [
                    {
                        image_id: ,
                        image_name: ,
                        image_description: ,
                        image_type: ,
                        image_os: ,
                        date_uploaded: ,
                        date_last_used: 
                    },
                ]
        """
        query = (
            'SELECT image_id, image_type, image_os, '
            'image_name, image_description, '
            'date_uploaded, date_last_used FROM images '
            'WHERE user_id = %s'
        )

        cursor = self.connection.cursor(prepared=True)
        cursor.execute(query, (user_id,))

        results = list(cursor.fetchall())

        results_dicts = [
            {
                'image_id': image_id,
                'image_name': image_name,
                'image_description': image_description,
                'image_type': image_type,
                'image_os': image_os,
                'date_uploaded': date_uploaded,
                'date_last_used': date_last_used
            } for (
                image_id,
                image_type,
                image_os,
                image_name,
                image_description,
                date_uploaded,
                date_last_used
            ) in results
        ]

        cursor.close()
        return results_dicts

    def create_job(self, test_id, scheduled_time):
        """
        Create a scheduled job

        Args:
            test_id (int): ID of test configuration
            scheduled_time (str): string representation of job start time
        
        Returns:
            id (int): job ID
        """
        query = (
            'INSERT INTO jobs '
            '(test_id, scheduled_start_time, completed)'
            'VALUES (?, ?, 0)'
        )

        cursor = self.connection.cursor(prepared=True)
        cursor.execute(query, (test_id, scheduled_time))

        self.connection.commit()

        id = cursor.lastrowid
        cursor.close()
        return id

    def delete_job(self, job_id):
        """
        Delete a scheduled job

        Args:
            job_id (int): ID of job to delete
        """
        query = (
            'DELETE FROM jobs '
            'WHERE job_id = %s'
        )

        cursor = self.connection.cursor(prepared=True)
        cursor.execute(query, (job_id,))

        self.connection.commit()
        cursor.close()

    def get_all_jobs(self, completed=None):
        """
        Get all scheduled jobs

        Args:
            completed (bool): True to retrieve completed jobs, False to retrieve jobs that 
                have not yet completed
        
        Returns:
            list of dicts:
                [
                    {
                        job_id: ,
                        test_id: ,
                        user_id: ,
                        scheduled_start_time: ,
                        completed: ,
                        padding: ,
                        duration:
                    }
                ]
        """
        where = ''
        if completed is True:
            where = 'WHERE completed = 1 '
        elif completed is False:
            where = 'WHERE completed = 0 '

        query = (
            'SELECT job_id, jobs.test_id, user_id, '
            'scheduled_start_time, duration, completed '
            'FROM jobs '
            'INNER JOIN test_configurations '
            'ON jobs.test_id = test_configurations.test_id '
            + where +
            'ORDER BY scheduled_start_time DESC'
        )

        cursor = self.connection.cursor()
        cursor.execute(query)
        results = cursor.fetchall()

        cursor.close()
        return [{
            'job_id': job_id,
            'test_id': test_id,
            'user_id': user_id,
            'scheduled_start_time': start_time.isoformat(),
            'completed': completed == 1,
            'padding': self.padding,
            'duration': duration
        } for (job_id,
               test_id,
               user_id,
               start_time,
               duration,
               completed) in results]

    def get_user_jobs(self, user_id, completed=None):
        """
        Get all scheduled jobs for a specified user

        Args:
            user_id (int): user ID to retrieve jobs for
            completed (bool): True to retrieve completed jobs, False to retrieve jobs that 
                have not yet completed
        
        Returns:
            list of dicts:
                [
                    {
                        job_id: ,
                        test_id: ,
                        scheduled_start_time: ,
                        completed: 
                    }
                ]
        """
        where = ''
        if completed is True:
            where = 'AND completed = 1 '
        elif completed is False:
            where = 'AND completed = 0 '

        query = (
            'SELECT job_id, jobs.test_id, '
            'scheduled_start_time, duration, completed '
            'FROM jobs '
            'INNER JOIN test_configurations '
            'ON jobs.test_id = test_configurations.test_id '
            'WHERE user_id = %s '
            + where +
            'ORDER BY scheduled_start_time DESC'
        )

        cursor = self.connection.cursor(prepared=True)
        cursor.execute(query, (user_id,))
        results = cursor.fetchall()

        cursor.close()

        # Convert time using isoformat() so the front end does not try to
        # compenstate for local time - we've done that already on the
        # backend!
        return [{
            'job_id': job_id,
            'test_id': test_id,
            'scheduled_start_time': start_time.isoformat(),
            'completed': completed == 1,
            'duration': duration
        } for (job_id, test_id, start_time, duration, completed) in results]

    def get_test_id(self, job_id):
        """
        Get test ID for a given job ID

        Args:
            job_id (int): job ID to get test ID for

        Returns:
            test_id (int): test ID for job ID
        """
        query = (
            'SELECT test_id FROM jobs '
            'WHERE job_id = %s '
            'LIMIT 1'
        )

        cursor = self.connection.cursor(prepared=True)
        cursor.execute(query, (job_id,))

        results = cursor.fetchone()
        if not results:
            return None

        (test_id,) = results

        cursor.close()
        return test_id

    def get_job_info(self, job_id):
        """
        Get information about a job for the given ID 
        
        Args:
            job_id (int): ID of job to get information for
        
        Returns:
            dict:
            {
                job_id: ,
                test_id: ,
                user_email: ,
                user_username: ,
                test_name: ,
                test_description: ,
                completed: 
            }
        """
        query = (
            'SELECT jobs.test_id, user_email, user_username, '
            'test_name, test_description, duration, completed '
            'FROM jobs INNER JOIN test_configurations '
            'ON jobs.test_id = test_configurations.test_id '
            'INNER JOIN users '
            'ON test_configurations.user_id = users.user_id '
            'WHERE job_id = %s '
            'LIMIT 1'
        )

        cursor = self.connection.cursor(prepared=True)
        cursor.execute(query, (job_id,))
        results = cursor.fetchone()
        if not results:
            return None
        (test_id, user_email, user_username,
         test_name, test_description, duration, completed) = results

        cursor.close()

        return {
            'job_id': job_id,
            'test_id': test_id,
            'user_email': user_email,
            'user_username': user_username,
            'test_name': test_name,
            'test_description': test_description,
            'completed': completed == 1,
            'duration': duration
        }

    def mark_job_completed(self, job_id):
        """
        Mark the job given by the ID as completed in the database

        Args:
            job_id (int): job ID to mark as completed
        """
        query = (
            'UPDATE jobs '
            'SET completed = 1 '
            'WHERE job_id = %s'
        )

        cursor = self.connection.cursor(prepared=True)
        cursor.execute(query, (job_id,))

        self.connection.commit()
        cursor.close()

    def get_next_job(self):
        """
        Get the next scheduled job

        Returns:
            Tuple: (test ID, string representation of start time) if another job scheduled
                else (None, None)
        """
        # get the first job that doesn't have completed set
        query = (
            'SELECT test_id, scheduled_start_time '
            'FROM jobs '
            'WHERE completed = 0 '
            'ORDER BY scheduled_start_time DESC '
            'LIMIT 1'
        )

        cursor = self.connection.cursor()
        cursor.execute(query)

        results = cursor.fetchone()
        # All tests are complete, we can schedule tests ASAP
        if not results:
            return None, None

        cursor.close()
        return results

    def create_iot_controller(self,
                              user_id,
                              name,
                              description,
                              checksum):
        """
        Create a record of an IoT controller script

        Args:
            user_id (int): user that uploaded the script
            name (str): name of IoT controller script
            description (str): description of IoT conbtroller script
            checksum (str): script checksum
        
        Returns:
            id (int): ID of IoT controller script
        """
        query = (
            "INSERT INTO iot_controllers"
            "(user_id, iot_controller_checksum, "
            "iot_controller_description, iot_controller_name, "
            "date_uploaded, date_last_used) VALUES"
            "(%s, %s, %s, %s, NOW(), NOW())"
        )

        cursor = self.connection.cursor(prepared=True)
        cursor.execute(query, (user_id, checksum,
                               description, name))
        self.connection.commit()

        id = cursor.lastrowid
        cursor.close()
        return id

    def delete_iot_controller(self, iot_controller_id):
        """
        Delete the record of the IoT controller script given by the ID

        Args:
            iot_controller_id (int): ID of the controller script record to be deleted
        """
        query = (
            "DELETE FROM iot_controllers "
            "WHERE iot_controller_id = %s"
        )

        cursor = self.connection.cursor(prepared=True)
        cursor.execute(query, (iot_controller_id,))
        self.connection.commit()
        cursor.close()

    def get_iot_controllers(self, user_id):
        """
        Get the uploaded IoT controller scripts for the given user

        Args:
            user_id (int): user ID to get the IoT controller scripts for
        
        Returns:
            list of dicts:
                [
                    {
                        iot_controller_id: ,
                        iot_controller_name: ,
                        iot_controller_description: ,
                        date_uploaded: ,
                        date_last_used: 
                    }
                ]
        """
        query = (
            'SELECT iot_controller_id, '
            'iot_controller_name, iot_controller_description, '
            'date_uploaded, date_last_used FROM iot_controllers '
            'WHERE user_id = %s'
        )

        cursor = self.connection.cursor(prepared=True)
        cursor.execute(query, (user_id,))

        results = list(cursor.fetchall())

        results_dicts = [
            {
                'iot_controller_id': iot_controller_id,
                'iot_controller_name': iot_controller_name,
                'iot_controller_description': iot_controller_description,
                'date_uploaded': date_uploaded,
                'date_last_used': date_last_used
            } for (
                iot_controller_id,
                iot_controller_name,
                iot_controller_description,
                date_uploaded,
                date_last_used
            ) in results
        ]

        cursor.close()

        return results_dicts
    
    def check_iot_controller_uploaded(self, iot_controller_id):
        """
        Check to ensure the given IoT controller ID has been uploaded

        Args:
            iot_controller_id (int): controller ID to check for
        
        Returns:
            True if found
                else False
        """
        query = f'SELECT iot_controller_id FROM iot_controllers;'
        cursor = self.connection.cursor()
        cursor.execute(query)

        ids = [id for (id,) in cursor]

        cursor.close()

        if iot_controller_id in ids:
            return True
        else:
            return False

    def update_images_date_last_used(self, image_id):
        """
        Update the time of last use for a firmware image to now

        Args:
            image_id (int): image ID to update
        """
        #Set date_last_used of the image to now
        query = (
            'UPDATE images '
            'SET date_last_used = NOW() '
            'WHERE image_id = %s'
        )

        cursor = self.connection.cursor(prepared=True)
        cursor.execute(query, (image_id,))
        cursor.close()

    def update_iot_controller_date_last_used(self, test_controller_id):
        """
        Update the time of last use for a IoT controller script to now

        Args:
            test_controller_id (int): controller script ID to update
        """
        #Set date_last_used of the IoT controller to now
        query = (
            'UPDATE iot_controllers '
            'SET date_last_used = NOW() '
            'WHERE iot_controller_id = %s'
        )

        cursor = self.connection.cursor(prepared=True)
        cursor.execute(query, (test_controller_id,))
        cursor.close()

    def remove_old_data(self):
        """
        Execute the data retention policy:
            - Delete test configurations older that 1 year
            - Delete scheduled jobs that were scheduled to start over 1 year ago
            - Delete IoT controllers that were last used over 1 year ago
            - Delete firmware images that were last used over 30 days ago
        """
        #Fetch all tests that are at least 1 year old
        query = (
            'SELECT test_id FROM test_configurations '
            'WHERE DATE(date_uploaded) < (curdate() - INTERVAL 1 YEAR)'
        )

        cursor = self.connection.cursor(prepared=True)
        cursor.execute(query)

        results = list(cursor.fetchall())

        for result in results:
            os.remove(f'uploads/tests/{result[0]}.yaml') #Delete them from the uploads directory

        #Delete all tests that are at least 1 year old
        query = (
            'DELETE FROM test_configurations '
            'WHERE DATE(date_uploaded) < (curdate() - INTERVAL 1 YEAR)'
        )

        cursor.execute(query)

        #Delete all jobs that are at least 1 year old
        query = (
            'DELETE FROM jobs '
            'WHERE DATE(scheduled_start_time) < (curdate() - INTERVAL 1 YEAR)'
        )

        cursor.execute(query)

        #Fetch all IoT controllers that have been inactive for more than 1 year
        query = (
            'SELECT iot_controller_id FROM iot_controllers '
            'WHERE DATE(date_last_used) < (curdate() - INTERVAL 1 YEAR)'
        )

        cursor.execute(query)

        results = list(cursor.fetchall())

        for result in results:
            os.remove(f'uploads/iot_controllers/{result[0]}.py') #Delete them from the uploads directory
            print(f'Deleting IoT {result[0]}')

        #Delete all IoT controllers that have been inactive for more than 1 year
        query = (
            'DELETE FROM iot_controllers '
            'WHERE DATE(date_last_used) < (curdate() - INTERVAL 1 YEAR)'
        )

        cursor.execute(query)

        #Fetch all images that have been inactive for more than 30 days
        query = (
            'SELECT image_id FROM images '
            'WHERE DATE(date_last_used) < (curdate() - INTERVAL 30 DAY)'
        )

        cursor.execute(query)

        results = list(cursor.fetchall())

        for result in results:
            os.remove(f'uploads/images/{result[0]}-*') #Delete them from the uploads directory

        #Delete all images that have been inactive for more than 30 days
        query = (
            'DELETE FROM images '
            'WHERE DATE(date_last_used) < (curdate() - INTERVAL 30 DAY)'
        )

        cursor.execute(query)

        self.connection.commit()
        cursor.close()
