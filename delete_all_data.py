from influxdb import InfluxDBClient


client = InfluxDBClient(host='localhost', port=8086)

db_name = 'logging'

# delete and then recreate database
client.drop_database(db_name)
client.create_database(db_name)
