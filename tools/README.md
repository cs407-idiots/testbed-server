# Firmware Conversion Tools

This directory contains the pieces of the compiler toolchain to convert compiled firmware between different file formats. The architectures supported here (as shamelessly stolen from the Ubuntu pre-compiled PPA repositories) are Linux-X86 and armhf ONLY. You must add support for any other architectures such as Darwin.

## `arm-none-eabi-objcopy` 
From the ARM Embedded Compiler Toolchain, used for the Zolertia RE-Mote Rev-B. Conversion occurs from `.elf` to `.hex`. Should work for any CC2538-based platform.

## `msp430-objcopy`
From the MSP430 compiler toolchain, used for TelosB motes. Conversion occurs from `.elf` and `.bin` to `.hex`. Note that when converting from `.bin` the address must be shifted.