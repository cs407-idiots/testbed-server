import yaml
import sys
import os
import atexit

from flask import Flask
from flask_cors import CORS, cross_origin
from flask_mail import Mail
from database import InfluxDatabase, MySQLDatabase
from noderegister import NodeRegister
from mqtt_client import MQTTClient
from email_client import EmailClient
from scheduler import Scheduler

from routes import routes
from test_routes import test_routes

from apscheduler.schedulers.background import BackgroundScheduler

from waitress import serve

UPLOAD_FOLDER = 'uploads/'

# NOTE: Flask is running in single-threaded mode. One ID will be
# processed at a time.

def load_config():
    try:
        with open('config.yaml') as file:
            config = yaml.load(file, Loader=yaml.FullLoader)
            return config
    except OSError:
        print("config.yaml not found. "
              "Please create it in the current directory "
              "and re-run the script.")
        sys.exit()


def run():
    SECRET = os.urandom(24)

    config = load_config()
    if 'upload-folder' not in config:
        raise Exception('Expected upload-folder in config')

    full_upload_folder = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        config['upload-folder'])
    config['full-upload-path'] = full_upload_folder

    app = Flask(__name__)
    CORS(app)  # allow all origins

    # BETWEEN_TEST_PADDING - Time in seconds as padding before and after
    # concurrent tests:
    BETWEEN_TEST_PADDING = config['padding']

    # START_TEST_BUFFER - Time in seconds as a buffer for when a test should
    # start when the next avaliable slot is now:
    START_TEST_BUFFER = config['buffer']

    influx_db = InfluxDatabase(config)
    mysql_db = MySQLDatabase(config)

    node_register = NodeRegister(config)

    mail_settings = {
        'MAIL_SERVER': config['email']['host'],
        'MAIL_PORT': config['email']['port'],
        'MAIL_USE_TLS': False,
        'MAIL_USE_SSL': False,
        'MAIL_USERNAME': config['email']['username'],
        'MAIL_PASSWORD': config['email']['password']
    }

    app.config.update(mail_settings)
    mail = Mail(app)

    email_client = EmailClient(config, mail, mysql_db, app)

    mqtt_client = MQTTClient(config,
                             influx_db,
                             mysql_db,
                             node_register,
                             email_client)

    scheduler = Scheduler(config,
                          influx_db, mysql_db,
                          node_register,
                          mqtt_client,
                          email_client,
                          BETWEEN_TEST_PADDING,
                          START_TEST_BUFFER)

    mqtt_client.scheduler = scheduler

    app.node_register = node_register
    app.influx_db = influx_db
    app.mysql_db = mysql_db
    app.mqtt_client = mqtt_client
    app.scheduler = scheduler

    app.config['UPLOAD_FOLDER'] = full_upload_folder
    app.secret_key = SECRET

    app.register_blueprint(routes)

    if config['testing']:
        app.register_blueprint(test_routes)

    tests_rescheduled = app.scheduler.reschedule_incomplete_tests_on_startup()
    if tests_rescheduled == 0:
        mqtt_client.send_default_run_signal()

    
    # Data Retention:
    app.mysql_db.remove_old_data() # Initial call
    drp_scheduler = BackgroundScheduler()
    drp_scheduler.add_job(func=app.mysql_db.remove_old_data, trigger="interval", days=1)
    drp_scheduler.start()

    # For the WSGI configuration
    # Set threads to 1 as multi-threaded instance
    # causes crashes when accessing the MySQL database
    serve(app, host='0.0.0.0', port=8080, threads=1)

    def exit_handler():
        influx_db.close()
        mysql_db.close()
        mqtt_client.close()
        drp_scheduler.shutdown()
    atexit.register(exit_handler)


if __name__ == '__main__':
    run()
