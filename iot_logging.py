import sys
import os
import pexpect
import requests
import yaml

import subprocess as sp
import pandas as pd
from datetime import datetime
from dateutil.parser import parse
from datetimerange import DateTimeRange

def load_config():
    """
    Load config.yaml in order to dynamically retrieve the host and port
    values for the openHAB instance (taken from server.py).
    
    Returns:
        config (dict): The parsed config.yaml file
    """
    try:
        with open('config.yaml') as file:
            config = yaml.load(file, Loader=yaml.FullLoader)
            return config
    except OSError:
        print("config.yaml not found. "
              "Please create it in the current directory "
              "and re-run the script.")
        sys.exit()

OPENHAB_LOGDIR = str(os.environ['OPENHAB_LOGDIR'])
OPENHAB_URL = 'http://' + str(load_config()['openhab']['host'] + ':' + str(load_config()['openhab']['port']))

def get_pretty_name(id):
    """
    Queries openHAB's REST API to traverse through the list of registered
    things, given the passed ID. Then uses the returned dictionary to build a
    more meaningful name for the item.
    
    Args:
        id (int): The ID associated with the queried smart device.
    
    Returns:
        pretty_name (str): The result of the query that shows its 'pretty' name.
    """
    url = OPENHAB_URL + '/rest/things'
    r = requests.get(url = url).json()

    for thing in r:
        pretty_name = thing["label"]
        channels = thing["channels"]
        for channel in channels:
            linked_items = channel["linkedItems"]
            if len(linked_items) > 0 and id in linked_items:
                return pretty_name
    return id


def get_smart_device_name(id):
    """
    Queries openHAB's REST API to retrieve the label assigned to a smart
    device, given its ID. This also calls get_pretty_name(id).
    
    Args:
        id (int): The ID associated with the queried smart device.
    Returns:
        str: The device's name in the format pretty_name : label.
    """
    url = OPENHAB_URL + '/rest/items'
    params = {'fields' : 'name, label'}

    r = requests.get(url = url, params = params).json()

    for item in r:
        if id == item['name']:
            return get_pretty_name(id) + ': ' + item['label']

    return id

def get_iot_logs(job_id, start_time, end_time):
    """
    The returned list constitutes the IoT logs for LucidLab. This function 
    gets called at the end of a test that has used an IoT controller, and works 
    by parsing openHAB's events.log file, upon filtering by start and end time.
    
    Args:
        job_id (int): The job ID associated with the finished test.
        start_time (str): The start time of the test.
        end_time (str): The end time of the test.
    
    Returns:
        logs_dict_array (list(dict)): The logs as parsed from events.log
    """
    logs_file = open(OPENHAB_LOGDIR + '/events.log', 'r')

    dates = []
    thing_ids = []
    thing_names = []
    states_before = []
    states_after = []
    logs_dict_array = []

    for line in logs_file:
        if  'ItemStateChangedEvent' in line:
            line = line.replace('\n', '')
            date = line[: line.find('[ItemStateChangedEvent]')]
            try:
                date = parse(date)
            except ValueError:
                print("Error parsing date")

            if date not in DateTimeRange(start_time, end_time):
                continue
            else:
                thing_id = line[line.find('] -') + 3 : line.find('changed')].strip()
                thing_name = ''
                try:
                    thing_name = get_smart_device_name(thing_id)
                except:
                    thing_name = 'n/a'

                state_before = line[line.find(' from ') + 6 : line.find('to')].strip()
                state_after = line[line.find(' to ') + 4 :].strip()

                logs_dict_array.append({"measurement": "logging",
                                "tags": {
                                    "job_id": job_id,
                                    "log_type": "IOT",
                                    "thing_id": thing_id,
                                    "thing_name": thing_name,
                                    "state_before": state_before,
                                    "state_after": state_after
                                },
                                'time': date,
                                'fields': {
                                    "message": "openHAB log"
                                }
                })

    return logs_dict_array